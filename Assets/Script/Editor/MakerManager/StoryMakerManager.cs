﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoryMaker
{
    using Soulark.Utility;
    using UnityEditor;

    public class DrawNodeCurve
    {
        public bool isDrawing = false;

        public bool isSelectStart = false;
        public bool isSelectNext = false;

        public BaseNode StartInfo = null;
        public BaseNode NextInfo = null;
    }

    public enum NodeType
    {
        None,
        Start,
        Move,
        Bounce,
        Shake,
        Jump,
        Color,
        SpineAni,
        Swap,
        Active,
        Scale,
        Rotation,
        FadeInOut,
        Enable,
        End
    }

    public enum NodeDrawPos
    {
        Start,
        Next
    }

    public static class StoryMakerManager
    {
        
        public const string PlusButtonPath = "Assets/Datas/StoryMaker/Texture/personalStoreUI_icon_plus.prefab";

        public const string MakerModelPath = "Assets/Datas/StoryMaker/Default/StoryMakerModel.prefab";

        /// <summary>
        /// 프랍 프리팹 경로
        /// </summary>
        public const string PropPrefabPath = "Assets/Datas/StoryMaker/Default/StoryMakerProp.prefab";

        /// <summary>
        /// 스탭 프리팹 리소스 경로
        /// </summary>
        public const string StepPrefabPath = "Assets/Datas/StoryMaker/Default/StepObj.prefab";

        /// <summary>
        /// 연출 오브젝트 경로
        /// </summary>
        public const string DirectionPrefabPath = "Assets/Datas/StoryMaker/Default/DirectionInfoObj.prefab";

        /// <summary>
        /// 말풍선 프리팹 리소스 경로
        /// </summary>
        public const string TextBallonPrefabPath = "Assets/Datas/StoryMaker/Default/TextBallon.prefab";

        /// <summary>
        /// Text 프리팹 리소스 경로
        /// </summary>
        public const string TextPrefabPath = "Assets/Datas/StoryMaker/Default/StoryMakerTextProp.prefab";

        /// <summary>
        /// Image 프리팹 리소스 경로
        /// </summary>
        public const string ImagePrefabPath = "Assets/Datas/StoryMaker/Default/StoryMakerImageProp.prefab";
        
        /// <summary>
        /// 노드 파란색 버튼 이미지
        /// </summary>
        public const string Texture_NodeYellow = "Assets/Datas/StoryMaker/Texture/UI/node_button_yellow.png";

        /// <summary>
        /// 노드 파란색 버튼 이미지
        /// </summary>
        public const string Texture_NodeWhite = "Assets/Datas/StoryMaker/Texture/UI/node_button_white.png";
        
        /// <summary>
        /// 디테일화면 스크롤을 강제로 생성시켜주는 텍스트
        /// </summary>
        public const string String_DetailScroll = "                                                                ";

        /// <summary>
        /// Save 버튼 이미지
        /// </summary>
        public const string Texture_SaveButton = "Assets/Datas/StoryMaker/Texture/UI/SaveResource.png";

        /// <summary>
        /// 좌측 , 우측 리스트 윈도우 아이디
        /// </summary>
        public const int Id_Listwindow = 1;
        public const int Id_DetailWindow = 2;

        /// <summary>
        /// 노드 그리는 부분에서 좌측 글자의 가로 넓이
        /// </summary>
        public static GUILayoutOption GUIStyle_NodeTextWidth = GUILayout.Width(85);

        /// <summary>
        /// 설명 스크립트 텍스트 높이값
        /// </summary>
        public static GUILayoutOption GUIStyle_DiscriptionHeight = GUILayout.Height(30);

        /// <summary>
        /// 현재 노드를 그리는중인지 체크
        /// </summary>
        public static DrawNodeCurve DrawingInfo = new DrawNodeCurve();

        public static void DrawNodeLineReset()
        {
            DrawNodeLineEnd();
        }

        public static void DrawNodeLineEnd()
        {
            if (null == DrawingInfo)
                DrawingInfo = new DrawNodeCurve();

            if (true == DrawingInfo.isDrawing)
            {
                DrawingInfo.isDrawing = false;
                DrawingInfo.isSelectStart = false;
                DrawingInfo.isSelectNext = false;

                DrawingInfo.StartInfo = null;
                DrawingInfo.NextInfo = null;
            }
        }

        public static bool isDrawing()
        {
            if (true == DrawingInfo.isDrawing)
                return true;

            return false;
        }

        public static void SetStartNode(BaseNode startNode, NodeDrawPos startPos)
        {
            if(NodeDrawPos.Start == startPos)
                DrawingInfo.StartInfo = startNode;
            else
                DrawingInfo.NextInfo = startNode;

            DrawingInfo.isDrawing = true;
        }

        public static Rect GetNodeCurveStartRect()
        {
            Rect nodeStartPos = new Rect(0,0,0,0);
            if(true == DrawingInfo.isSelectStart)
            {
                nodeStartPos = DrawingInfo.StartInfo.DrawRect;
                nodeStartPos.x += -10;
                nodeStartPos.y += DrawingInfo.StartInfo.DrawRect.height / 2;
            }
            else if(true == DrawingInfo.isSelectNext)
            {
                nodeStartPos = DrawingInfo.NextInfo.DrawRect;
                nodeStartPos.x += DrawingInfo.NextInfo.DrawRect.width + 10;
                nodeStartPos.y += DrawingInfo.NextInfo.DrawRect.height / 2;
            }

            return nodeStartPos;
        }

        public static GUIStyle GetNodeContent(bool isMulty, bool isFreeze)
        {
            if(true == isMulty)
            {
                if(true == isFreeze)
                    return "flow node 0 on";
                else
                    return "flow node 0";
            }
            else
            {
                if (true == isFreeze)
                    return "flow node 1 on";
                else
                    return "flow node 1";
            }
        }

        public static Rect GetBackGroundBoxPos(Rect drawRect)
        {
            Rect returnRect = new Rect(drawRect.x, drawRect.y, drawRect.width, drawRect.height);
            return returnRect;
        }

        public static Rect GetLeftButtonPos(Rect drawRect)
        {
            Rect returnRect = new Rect(0,0,0,0);
            returnRect.x = drawRect.x - 20;
            returnRect.y = drawRect.y + (drawRect.height / 2) - 10;
            returnRect.width = 20;
            returnRect.height = 20;

            return returnRect;
        }

        public static Rect GetRightButtonPos(Rect drawRect)
        {
            Rect returnRect = new Rect(0, 0, 0, 0);
            returnRect.x = drawRect.x + drawRect.width;
            returnRect.y = drawRect.y + (drawRect.height / 2) - 10;
            returnRect.width = 20;
            returnRect.height = 20;

            return returnRect;
        }

        public static bool IsSamePosition(NodeDrawPos pos)
        {
            switch(pos)
            {
                case NodeDrawPos.Next:
                    {
                        
                        if (DrawingInfo.isSelectNext == true)
                            return true;
                    }
                    break;
                case NodeDrawPos.Start:
                    {
                        if (DrawingInfo.isSelectStart == true)
                            return true;
                    }
                    break;
                default:
                    break;
            }
            return false;
        }

        public static Texture GetTexture(string path)
        {
            Texture findTexture = AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D)) as Texture2D;
            if (null == findTexture)
                return null;

            return findTexture;
        }
    }
}



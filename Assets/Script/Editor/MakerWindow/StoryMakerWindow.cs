﻿using UnityEngine;
using UnityEditor;

namespace StoryMaker
{
    public partial class StoryMakerWindow : EditorWindow
    {
        StoryMakerModel Model;

        bool isInitialize = false;

        Rect window_listRect = new Rect(10, 10, 270, 300);
        Rect window_DetailRect = new Rect(290, 10 + 40, 250, 300);
        Rect StepNameTitleRect = new Rect(300, 10, 500, 70);
        Rect StepNameRect = new Rect(535, 10, 500, 70);
        Rect SaveButtonRect = new Rect(535, 5, 40, 40);
        
        GUIStyle fontStyle = new GUIStyle();

        [MenuItem("Tools/StoryMaker", priority = 1000)]
        public static void ShowWindow()
        {
            StoryMakerWindow window = GetWindow<StoryMakerWindow>();
            if (null != window)
            {
                window.minSize = new Vector2(260, 300);
                window.Initialize();
            }
        }

        private void OnEnable()
        {
            if (null == Model)
                CreateModelObj();
        }

        public void Initialize()
        {
            if (true == isInitialize)
                return;

            CreateModelObj();
            isInitialize = true;
        }

        void CreateModelObj()
        {
            if (null == Model)
            {
                GameObject findModel = GameObject.Find("StoryMakerModel");
                if (null == findModel)
                {
                    GameObject resource = AssetDatabase.LoadAssetAtPath(StoryMakerManager.MakerModelPath, typeof(GameObject)) as GameObject;
                    if (null != resource)
                    {
                        GameObject newModel = Instantiate(resource);
                        newModel.name = "StoryMakerModel";
                        Model = newModel.GetComponent<StoryMakerModel>();
                    }
                }
                else
                    Model = findModel.GetComponent<StoryMakerModel>();
            }

            //Model.Initialize();
            StoryMakerManager.DrawNodeLineReset();
        }

        public void OnGUI()
        {
            if (false == isInitialize)
                return;

            if (null == Model)
                CreateModelObj();
            else
            {
                BeginWindows();
                // Draw List Window
                window_listRect.height = position.height - 20;
                window_listRect = GUI.Window(StoryMakerManager.Id_Listwindow, window_listRect, OnDraw_ListWindowNew, "List Viewer");

                GUI.color = Color.white;

                // Draw Detail Window
                window_DetailRect.width = position.width - 300;
                window_DetailRect.height = position.height - 60;

                window_DetailRect = GUI.Window(StoryMakerManager.Id_DetailWindow, window_DetailRect, OnDraw_DetailWindow, "Detail Viewer");

                EndWindows();

                OnDraw_OtherUI();
            }
           
            OnMouseClick_KeyboardFocusOut();
        }

        void OnDraw_OtherUI()
        {
            fontStyle.fontSize = 25;
            fontStyle.normal.textColor = Color.white;
            GUI.Label(StepNameTitleRect, "StoryProp Name : ", fontStyle);

            if (null == Model.GetSelectProp())
            {
                fontStyle.fontSize = 25;
                fontStyle.normal.textColor = Color.red;

                GUI.Label(StepNameRect, "Empty", fontStyle);
            }
            else
            {
                fontStyle.fontSize = 25;
                fontStyle.normal.textColor = Color.yellow;
                GUI.Label(StepNameRect, Model.GetSelectProp().name, fontStyle);
            }

            SaveButtonRect.x = position.width - 50;

            if (GUI.Button(SaveButtonRect, StoryMakerManager.GetTexture(StoryMakerManager.Texture_SaveButton)))
            {
                if (null == Model.GetSelectProp())
                {
                    EditorUtility.DisplayDialog("Save Error",
                        "저장할 정보가 없습니다.", "OK", "");
                    return;
                }
                if (false == IsHaveSelectStep())
                {
                    EditorUtility.DisplayDialog("Save Error",
                        "저장할 정보가 없습니다.", "OK", "");
                    return;
                }

                SaveStepInfo();
            }
        }

        void OnDraw_StepButtons(StepInfo info)
        {
            if (GUILayout.Button("ADD String Ani Text"))
            {
                //AddResourceInfo_Text(info);
            }
            if (GUILayout.Button("ADD TextBallon Image"))
            {
                //AddResourceInfo_BallonImage(info);
            }

            /// 스탭인포 삭제
            GUI.color = Color.red;
            if (GUILayout.Button("Delete"))
            {
                Model.GetStepList().Remove(info);

                Transform parentObj = info.TextBallonImageObj.transform.parent;
                if (null != parentObj)
                    DestroyImmediate(parentObj.gameObject);
            }
            GUI.color = Color.white;

            GUILayout.Space(3);
        }

        /// <summary>
        /// 활성화 및 연출 시간
        /// </summary>
        void OnDraw_Duration(StepInfo info)
        {
            info.Duration = EditorGUILayout.FloatField("Duration", info.Duration);
            info.EndString = EditorGUILayout.TextField("End String", info.EndString);
        }

        void OnMouseClick_KeyboardFocusOut()
        {
            Event e = Event.current;
            if (null == e)
                return;
            
            if (e.button == 1 && e.type == EventType.MouseDown)
            {
                GUIUtility.keyboardControl = 0;
            }
            else if (e.button == 0 && e.type == EventType.MouseDown)
            {
                GUIUtility.keyboardControl = 0;
            }
        }
    }
}
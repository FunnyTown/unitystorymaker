﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

namespace StoryMaker
{
    /// <summary>
    /// 좌측 리스트를 출력해주는 코드
    /// </summary>
    partial class StoryMakerWindow : EditorWindow
    {
        Vector2 scrollPos;
        List<StepInfo> EditStepInfos = new List<StepInfo>();

        const string DefaultNameFieldName = "insert this !";
        string CreatePropName = "insert this !";

        /// <summary>
        /// 좌측 리스트 윈도우 그리기
        /// </summary>
        void OnDraw_ListWindowNew(int id)
        {
            if (Model.GetSelectProp() == null)
            {
                OnDraw_CreatePropUI();
                SelectedStep = null;
                return;
            }

            OnMouseClick_KeyboardFocusOut();
            OnDraw_SelectPropObj();
            OnDraw_AddStep();
            FindSteps();

            EditorGUILayout.Space();

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(window_listRect.width-10), GUILayout.Height(window_listRect.height-90));

            int stepNumber = 1;

            foreach (StepInfo stepinfo in new List<StepInfo>(Model.GetSelectProp().StepList))
            {
                if (null == stepinfo)
                    continue;

                GUILayout.Space(5f);
                OnDraw_StepField(stepinfo);
                FindDirections(stepinfo);
                
                foreach (DirectionInfo info in new List<DirectionInfo>(stepinfo.OtherDirectionInfos))
                {
                    GUILayout.Space(10f);

                    OnDraw_DirectionField(info);
                    
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(15f);

                    if (null == info.TargetObj)
                        EditorGUILayout.LabelField("TargetObj : 타겟을 지정해주세요.");
                    else
                        EditorGUILayout.LabelField("TargetObj : " + info.TargetObj.name);

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(15f);
                    EditorGUILayout.LabelField("Duration :" + info.Duration);
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(15f);
                    EditorGUILayout.LabelField("MultiPlay :" + info.IsMultiPlay);
                    EditorGUILayout.EndHorizontal();
                }

                stepNumber++;
                EditorGUILayout.Space();
            }
            
            EditorGUILayout.EndScrollView();
        }
        
        /// 스탭 정보 관련
        #region StepList Controller

        /// <summary>
        /// 스탭 선택 버튼
        /// </summary>
        void OnDraw_StepField(StepInfo stepInfo)
        {
            GUI.color = Color.green;

            GUIStyle drawStyle;

            if (SelectedStep == stepInfo)
                drawStyle = "ExposablePopupMenu";
            else
                drawStyle = "GV Gizmo DropDown";

            if (GUILayout.Button("Name : " + stepInfo.StepName, drawStyle, GUILayout.ExpandWidth(true) ))
            {
                ChangeSelectStepInfo(stepInfo);
            }

            GUI.color = Color.white;
        }

        void OnDraw_DirectionField(DirectionInfo directionInfo)
        {
            if(directionInfo.IsMultiPlay)
                GUI.color = Color.gray;
            else
                GUI.color = new Color32(119, 210, 255, 255);
            
            GUIStyle drawStyle = "BoldLabel";

            EditorGUILayout.BeginVertical();

            GUILayout.Label(directionInfo.DirectionType.ToString(), drawStyle, GUILayout.ExpandWidth(true));
            
            GUI.color = Color.white;

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(15f);
            GUILayout.Label("설명 : " + directionInfo.Discription, "ScriptText", GUILayout.ExpandWidth(true), GUILayout.MaxHeight(20));

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// 연출 삭제 버튼
        /// </summary>
        void OnDraw_DeleteDirection(StepInfo stepInfo, DirectionInfo info)
        {
            if(GUILayout.Button("Delete", GUILayout.Width(50)))
            {
                stepInfo.OtherDirectionInfos.Remove(info);
            }
        }

        /// <summary>
        /// 현재 선택된 프랍 오브젝트 , 여기다 끌어놓으면 계속 변경 가능
        /// </summary>
        void OnDraw_SelectPropObj()
        {
            Model.SetSelectProp((StoryProp)EditorGUILayout.ObjectField("Select Prop", Model.GetSelectProp(), typeof(StoryProp), true));
            if (null != Model.GetSelectProp())
            {
                string propName = Model.GetSelectProp().name;
                Model.GetSelectProp().name = EditorGUILayout.TextField("Prop Name", propName);
            }
        }

        /// <summary>
        /// 스탭 추가 버튼
        /// </summary>
        void OnDraw_AddStep()
        {
            if (GUILayout.Button("ADD Step"))
            {
                OnClick_NewStep();
            }
        }

        /// <summary>
        /// 현재 프랍의 자식으로 들어있는 스탭정보 가져오는부분
        /// </summary>
        void FindSteps()
        {
            GameObject propParent;
            if (null == Model.GetSelectProp())
                return;

            propParent = Model.GetSelectProp().gameObject;

            StepInfoObject[] AllSteps = propParent.GetComponentsInChildren<StepInfoObject>();

            EditStepInfos.Clear();

            for (int index = 0; index < AllSteps.Length; index++)
            {
                StepInfo newInfo = new StepInfo();
                newInfo = AllSteps[index].GetStepInfo();
                AllSteps[index].SetName(newInfo.StepName);

                EditStepInfos.Add(newInfo);
            }

            Model.GetSelectProp().StepList = EditStepInfos;
        }

        /// <summary>
        /// 현재 선택한 스탭의 연출 리스트를 가져오는 부분
        /// </summary>
        void FindDirections(StepInfo selectinfo)
        {
            StepInfoObject[] StepObjts = Model.GetSelectProp().GetComponentsInChildren<StepInfoObject>();
            StepInfoObject findStepObj = StepObjts.FirstOrDefault(d => d.GetComponent<StepInfoObject>().GetStepInfo() == selectinfo);
            if (null == findStepObj)
                return;

            DirectionInfoObject[] DirectionObjts = findStepObj.GetComponentsInChildren<DirectionInfoObject>();
            List<DirectionInfo> findDirectionList = new List<DirectionInfo>();

            for (int index = 0; index < DirectionObjts.Length; index++)
            {
                DirectionInfo newInfo = new DirectionInfo();
                newInfo = DirectionObjts[index].GetDirectionInfo();
                findDirectionList.Add(newInfo);
            }

            selectinfo.OtherDirectionInfos = findDirectionList;
        }

        void OnDraw_CreatePropUI()
        {
            GUIStyle fontStyle = new GUIStyle();
            fontStyle.fontSize = 15;
            fontStyle.normal.textColor = Color.white;
            
            GUILayout.Label("   HOW TO Setting Story Prop", fontStyle);
            GUILayout.Space(5);
            fontStyle.normal.textColor = Color.yellow;

            GUILayout.Label(" - Input StoryProp for Hierarchy", fontStyle);
            GUILayout.Space(10);
            OnDraw_SelectPropObj();

            GUILayout.Space(10);
            fontStyle.normal.textColor = Color.white;
            GUILayout.Label("                       O R", fontStyle);

            GUILayout.Space(15);
            fontStyle.normal.textColor = Color.yellow;
            

            GUILayout.Label(" - Insert PropName ", fontStyle);
            GUILayout.Label("             and press CreateButton", fontStyle);

            GUILayout.Space(5);
            CreatePropName = EditorGUILayout.TextField("Create StoryProp Name : ", CreatePropName);
            GUILayout.Space(5);

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(90);
            if(GUILayout.Button("Create", GUILayout.Width(80)))
            {
                CreateStoryProp();
            }
            EditorGUILayout.EndHorizontal();
        }

        void CreateStoryProp()
        {
            if(CreatePropName == DefaultNameFieldName)
            {
                EditorUtility.DisplayDialog("Create Fail", "생성할 StoryProp 이름을 적어주세요.", "OK", "");
                return;
            }

            string localPath = "Assets/BuildAssetBundlesSourcePath/StoryMaker/Props/" + CreatePropName + ".prefab";
            if (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject)))
            {
                EditorUtility.DisplayDialog("Create Fail", "같은 이름의 StoryProp이 존재합니다.\n다른이름으로 만드세요.", "OK", "");
                return;
            }

            OnClick_NewProp();
            OnClick_NewStep();
        }

        #endregion

        /// 기본 노드 셋팅 및 선택한 스탭 정보로 노드 변경해주는부분
        #region Default Node Setting

        /// <summary>
        /// 선택한 스탭 정보를 우측 노드 윈도우에 출력해주는 부분
        /// </summary>
        void ChangeSelectStepInfo(StepInfo selectInfo)
        {
            nodelist.Clear();

            if (SelectedStep != selectInfo)
                SelectedStep = selectInfo;

            if (null == selectInfo)
                return;

            CreateStartNode();

            float posX = 0, posY = 0;
            Rect getRect;
            posX = 160;
            posY = 200;

            bool isFirstMultiNode = true;


            foreach ( DirectionInfo info in new List<DirectionInfo>(selectInfo.OtherDirectionInfos))
            {
                if (true == info.IsMultiPlay)
                {
                    if(true == isFirstMultiNode)
                    {
                        isFirstMultiNode = false;
                        ChangeNodeList(info, posX, 355);
                    }
                    else
                        ChangeNodeList(info, posX, posY);

                    getRect = GetLastNodeRect();
                    posY = getRect.y + getRect.height + 30;

                    continue;
                }

                posY = 200;

                ChangeNodeList(info, posX);

                getRect = GetLastNodeRect();
                posX = getRect.x + getRect.width + 50;
                isFirstMultiNode = true;
            }

            CreateStepNode(posX);

            int nodeIndex = 0;
            BaseNode multinodeParent = null;
            foreach(BaseNode nodeItem in nodelist)
            {
                if(nodeItem.Type != NodeType.End)
                {
                    if(nodelist.Count != 2)
                    {
                        if (nodelist[nodeIndex + 1].Type != NodeType.End && true == nodelist[nodeIndex + 1].IsMulti())
                        {
                            if (null == multinodeParent)
                                multinodeParent = nodeItem;

                            multinodeParent.NextMultiNode.Add(nodelist[nodeIndex + 1]);
                            nodeIndex++;
                            continue;
                        }
                    }
                    
                    if (null != multinodeParent)
                    {
                        multinodeParent.NextNode = nodelist[nodeIndex + 1];
                        multinodeParent = null;
                    }
                    else
                    {
                        nodeItem.NextNode = nodelist[nodeIndex + 1];
                    }
                }
                nodeIndex++;
            }
        }

        /// <summary>
        /// Start 노드 생성
        /// </summary>
        void CreateStartNode()
        {
            BaseNode newNode = new HeaderNode();
            ((HeaderNode)newNode).Init();
            newNode.DrawRect.x += 10;
            newNode.NodeSelectAction_Right = OnClick_NodeNext;
            nodelist.Add(newNode);
        }

        /// <summary>
        /// End 노드 생성
        /// </summary>
        /// <param name="posX"></param>
        void CreateStepNode(float posX)
        {
            BaseNode newNode = new StepNode();
            ((StepNode)newNode).Init();
            ((StepNode)newNode).SelectedStep = SelectedStep;
            newNode.DrawRect.x = posX;
            newNode.NodeSelectAction_Left = OnClick_NodeStart;
            nodelist.Add(newNode);
        }
        
        /// <summary>
        /// 현재 선택한 스탭의 연출정보를 노드로 만들어주는 부분
        /// </summary>
        public void ChangeNodeList(DirectionInfo directionInfo, float posX = 0, float posY = 50)
        {
            BaseNode newNode = new BaseNode();

            switch (directionInfo.DirectionType)
            {
                case DirectionType.Move:
                    {
                        newNode = new MoveNode();
                        ((MoveNode)newNode).Init();
                    }
                    break;
                case DirectionType.Jump:
                    {
                        newNode = new JumpNode();
                        ((JumpNode)newNode).Init();
                    }
                    break;
                case DirectionType.ChangeColor:
                    {
                        newNode = new ColorNode();
                        ((ColorNode)newNode).Init();
                    }
                    break;
                case DirectionType.Bounce:
                    {
                        newNode = new BounceNode();
                        ((BounceNode)newNode).Init();
                    }
                    break;
                case DirectionType.Shake:
                    {
                        newNode = new ShakeNode();
                        ((ShakeNode)newNode).Init();
                    }
                    break;
                case DirectionType.Swap:
                    {
                        newNode = new SwapNode();
                        ((SwapNode)newNode).Init();
                    }
                    break;
                case DirectionType.Active:
                    {
                        newNode = new ActiveNode();
                        ((ActiveNode)newNode).Init();
                    }
                    break;
                case DirectionType.Rotation:
                    {
                        newNode = new RotationNode();
                        ((RotationNode)newNode).Init();
                    }
                    break;
                case DirectionType.Scale:
                    {
                        newNode = new ScaleNode();
                        ((ScaleNode)newNode).Init();
                    }
                    break;
                case DirectionType.FadeInOut:
                    {
                        newNode = new FadeInOutNode();
                        ((FadeInOutNode)newNode).Init();
                    }
                    break;
                case DirectionType.Enable:
                    {
                        newNode = new EnableNode();
                        ((EnableNode)newNode).Init();
                    }
                    break;
                default:
                    break;
            }

            if(null != newNode)
            {
                newNode.DrawRect.x = posX;
                newNode.DrawRect.y = posY;
                newNode.NodeSelectAction_Left = OnClick_NodeStart;
                newNode.NodeSelectAction_Right = OnClick_NodeNext;
                newNode.DirectionInfo = directionInfo;
                nodelist.Add(newNode);
            }
        }

        /// <summary>
        /// 마지막에 그린 노드 렉트 가져오기
        /// </summary>
        public Rect GetLastNodeRect()
        {
            if (nodelist.Count == 0)
                return new Rect(0,0,0,0);

            return nodelist[nodelist.Count - 1].DrawRect;
        }

        #endregion

        /// <summary>
        /// 새로운 스탭 추가
        /// </summary>
        public void OnClick_NewStep()
        {
            GameObject resourcePrefab = AssetDatabase.LoadAssetAtPath(StoryMakerManager.StepPrefabPath, typeof(GameObject)) as GameObject;
            if (null == resourcePrefab)
                return;

            GameObject newResObj = Instantiate(resourcePrefab, GetStepParent());

            var Component = newResObj.GetComponent<StepInfoObject>();
            if(null != Component)
            {
                if(null != Component.stepinfo)
                    Component.stepinfo.StepName = "new Step " + newResObj.transform.GetSiblingIndex();
            }
            newResObj.transform.localPosition = Vector3.zero;
        }

        /// <summary>
        /// 새로운 프랍 추가
        /// </summary>
        public void OnClick_NewProp()
        {
            GameObject resourcePrefab = AssetDatabase.LoadAssetAtPath(StoryMakerManager.PropPrefabPath, typeof(GameObject)) as GameObject;
            if (null == resourcePrefab)
                return;

            GameObject newResObj = Instantiate(resourcePrefab, GetUIContainer());
            newResObj.name = CreatePropName;
            newResObj.transform.localPosition = Vector3.zero;

            Model.selectProp = newResObj.GetComponent<StoryProp>();
        }

        Transform GetStepParent()
        {
            GameObject parent = Model.selectProp.gameObject.GetChildObject("StepObjects");
            if (null != parent)
                return parent.transform;
            else
                return Model.selectProp.transform;
        }

        public Transform GetUIContainer()
        {
            GameObject findObj = GameObject.Find("SoularkUI");
            if (null != findObj)
            {
                Transform containerObj = findObj.transform.Find("UI Container");
                if (null != containerObj)
                    return containerObj;
            }

            findObj = GameObject.Find("Canvas");
            if (null != findObj)
                return findObj.transform;

            return null;
        }
    }
}

﻿using UnityEditor;
using UnityEngine.UI;

namespace StoryMaker
{
    [CustomEditor(typeof(StepInfo))]
    public class StepInfoEditor : Editor
    {
        StepInfo _stepInfo = null;

        void OnEnable()
        {
            //_stepInfo = target as StepInfo;
        }

        public override void OnInspectorGUI()
        {
            SetBallonObjects();
            base.OnInspectorGUI();
        }

        void SetBallonObjects()
        {
            if (null == _stepInfo.BallonObj)
                return;

            if(null == _stepInfo.TextBallonImageObj)
                _stepInfo.TextBallonImageObj = _stepInfo.BallonObj.transform.Find("ImageBallon").GetComponent<Image>();
            
            if(null == _stepInfo.TextObj)
                _stepInfo.TextObj = _stepInfo.BallonObj.transform.Find("TextString").GetComponent<Text>();

            if (null == _stepInfo.TouchObj)
                _stepInfo.TouchObj = _stepInfo.BallonObj.transform.Find("ImageTouch").gameObject;
        }
    }
}

﻿namespace Soulark.Utility
{
    /// <summary>
    /// Table 에서 불러오는 ( TB_Text ) 텍스트 RefID 모음
    /// </summary>
    public class TableTextRef
    {
        /// <summary>
        /// 공용 
        /// </summary>
        public const int BattlePower = 80800;   //전투력
        public const int Attack = 80801;        //공격력
        public const int Defense = 80802;       //방어력
        public const int Hp = 80803;            //체력
        public const int Ok = 80830;            //확인
        public const int Cancel = 80831;        //취소


        /// <summary>
        /// SelfShop 개인상점
        /// </summary>
        public const int SelfShop_DeleteList = 70000219;                // 목록제거
        public const int SelfShop_Delete_List_How = 70000220;           // 아이템을 판매목록에서 제거하시겠습니까?
        public const int SelfShop_Delete = 70000221;                    // 제거
        public const int SelfShop_Cancle = 70000222;                    // 취소
        public const int SelfShop_Cannot_Add = 70000223;                // 더이상 추가할 수 없습니다.
        public const int SelfShop_Min_Price = 70000224;                 // 이 상품의 최소가격은 {0} 입니다.
        public const int SelfShop_Sell_Price = 70000225;                // 상품 가격을 입력해주세요.
        public const int SelfShop_Sell_Count = 70000226;                // 상품 판매 갯수를 입력해주세요.
        public const int SelfShop_Password_Fail = 70000228;             // 올바르지 않은 비밀번호입니다.
        public const int SelfShop_Ready_Contents = 70000227;            // 준비중인 기능입니다.
        public const int SelfShop_What = 70000229;                      // ~ 를
        public const int SelfShop_Won = 70000230;                       // 원
        public const int SelfShop_How = 70000231;                       // ~ 에
        public const int SelfShop_Count = 70000232;                     // 개
        public const int SelfShop_Buyed = 70000233;                     // 구매하셨습니다.
        public const int SelfShop_Enter_Item_Plz = 70000652;            // 아이템을 하나이상 등록해주세요.
        public const int SelfShop_Cannot_Open_Level = 70000611;         // 플레이어(계정) 레벨이 {0} 이상이어야 개인상점을 열수 있습니다.

        /// <summary>
        /// Shop 상점
        /// </summary>

        public const int Shop_Buy_Item_Question = 10004;                // 아이템을 구매하시겠습니까 ?
        public const int Shop_Refresh_ItemShop_Title = 10008;           // 상점 새로고침
        public const int Shop_Refresh_ItemShop_Context = 10009;         // 상점을 새로고침 하시겠습니까?
        public const int Shop_Allready_BuyItem = 10006;                 // 이미 구매한 상품입니다.
        public const int Shop_Complete_Buyitem = 10005;                 // 아이템을 구입했습니다.


        /// <summary>
        /// SoulChange 영혼 교환소
        /// </summary>
        public const int SoulChange_Select_Material = 70000235;                 // 재료조각을 선택해주세요
        public const int SoulChange_Immediate_Question = 70000236;              // 재화를 소모하여 즉시완료 하시겠습니까?
        public const int SoulChange_Cannot_Refresh = 70000237;                  // 추출 중일 때는 보상을 갱신할수 없습니다.
        public const int SoulChange_Refresh_Question = 70000238;                // 새로 고침 하시겠습니까?
        public const int SoulChange_Refresh_Time = 70000239;                    // 자동 리셋 시간
        public const int SoulChange_Refresh = 70000240;                         // 조각 보상 갱신
        public const int SoulChange_Nohave_Piece = 70000241;                    // 보유중인 조각이 없습니다.
        public const int SoulChange_Piece_Enough = 70000242;                    // 재료 조각이 부족합니다.
        public const int SoulChange_Refresh_RewardPiece_Complete = 70000234;    // 보상 조각이 갱신되었습니다.
        public const int SoulChange_Immediate = 70000541;                       // 즉시완료


        /// <summary>
        /// DailyDungeon 요일던전
        /// </summary>
        public const int DailyDungeon_Easy = 70000243;                    // 쉬움
        public const int DailyDungeon_Normal = 70000244;                  // 보통
        public const int DailyDungeon_Hard = 70000245;                    // 어려움
        public const int DailyDungeon_Gold_Dungeon = 70000247;            // 골드 던전
        public const int DailyDungeon_YanngGgoChi_Dungeon = 70000248;            // 양꼬치 던전
        public const int DailyDungeon_Data_Dungeon = 70000249;            // 데이터 던전
        public const int DailyDungeon_Need_Level_Notice = 70000250;       // {0}레벨 부터 입장하실수 있습니다.
        public const int DailyDungeon_Play_Count_End = 70000251;          // 이용횟수 모두 소진
        public const int DailyDungeon_No_Have_Ticket = 70000246;          // 티켓이 부족하여 다시시도 할수 없습니다.
        public const int DailyDungeon_Buy_Context = 70000649;             // 을(를) 구매하시겠습니까 ?
        public const int DailyDungeon_Buy_Ticket_Complete = 70000650;     // 티켓 구매 완료
        public const int DailyDungeon_Buy_Ticket_Popup_Title = 70000562;     // 충전


        /// <summary>
        /// SnackBar 포장마차
        /// </summary>
        public const int SnackBar_Day = 70000215;                     // 일
        public const int SnackBar_Hour = 70000216;                    // 시간
        public const int SnackBar_Munite = 70000217;                  // 분
        public const int SnackBar_Buy = 70000218;                     // 구매
        public const int SnackBar_Max_Cook_Count = 70000252;          // 더이상 요리를 할 수 없습니다.\N동시 제한 개수를 확인해주세요.
        public const int SnackBar_Immediate = 70000253;               // 재화를 소모하여 즉시완료 하시겠습니까?
        public const int SnackBar_Cook_LevelUp = 70000254;            // 요리 숙련도 레벨업
        public const int SnackBar_Need_Material = 70000626;           // 재료가 부족합니다.
        public const int SnackBar_Multi_Make_Count = 70000627;        // 동시 생산 개수
        public const int SnackBar_How_Make_Count = 70000628;          // 회 제작
        public const int SnackBar_Remain = 70000629;                  // 남음
        public const int SnackBar_Need_Time = 70000630;               // 소요
        public const int SnackBar_Complete = 70000631;                // 완성
        public const int SnackBar_Second = 70000632;                  // 초

        /// <summary>
        /// WorkShop 전파상
        /// </summary>
        public const int WorkShop_Ready_Contents = 70000227;            // 준비중인 기능입니다.

        /// <summary>
        /// SceneLoadManager
        /// </summary>
        public const int SceneLoad_Table_Init = 70000652;               // 데이터 초기화


        /// <summary>
        /// 타운
        /// </summary>
        public const int TownContents_Level_Limit = 70000653;           // 계정 {0} 레벨 이상 입장이 가능합니다.

        /// <summary>
        /// 타운 랭크
        /// </summary>
        public const int TownRank_Matching = 70000654;                  // 매칭
        public const int TownRank_MissionSlotName = 70000655;           // 미션 이름
        public const int TownRank_RewardName = 70000656;                // {0} 관왕

        /// <summary>
        /// UHGameCharacterSlot
        /// </summary>
        public const int NotEnough_FullPowerSkillPoint = 70000657;      // 권능 포인트가 부족합니다.
        public const int NotUsed_Skill = 70000658;                      // 사용할 수 없습니다.

        /// <summary>
        /// 폼폼 대전
        /// </summary>
        public const int Pompom_Ready = 70000659;                       // 폼폼들을 기다리고 있습니다.
        public const int Pompom_StartAlram = 70000660;                  // 잠시 후 폼폼 대전이 시작합니다.

        /// <summary>
        /// 체인 스킬
        /// </summary>
        public const int NotUsed_ChainSkill = 70000661;                                 // 사용할 수 없습니다.


        public const int TripleRaid_Zone = 70000662;                                    // {0}존
        public const int TripleRaid_Name = 70000663;                                    // 3인 레이드
        public const int TripleRaid_DefaultName = 70000664;                             // 훈련인
        public const int TripleRaid_NotEnterRoom_Title = 70001383;                      // 입장불가
        public const int TripleRaid_NotEnterRoom_Context = 70001384;                    // 입장할 수 있는 방이 없습니다.\n방을 생성하시겠습니까?

        public const int StoryMode_ChapterOpen = 70000665;                              // Chapter.{0} 오픈
        public const int StoryMode_HardChapterOpen = 70000666;                          // Hard Chapter.{0} 오픈
        public const int StoryMode_RewardTitle = 70000667;                              // 보상 일람
        public const int StoryMode_RewardInfo = 70000668;                               // 획득 가능한 보상입니다.
        public const int StoryMode_RewardConfirm = 70000669;                            // 확인
        public const int StoryMode_OpenCondition = 70000670;                            // 이전 챕터 클리어시 개방됩니다.
        public const int StoryMode_HardOpenCondition = 70000671;                        // 현재 챕터의 노멀난이도 클리어 시 개방됩니다.

        public const int TownMap_MoveEdit = 70000672;                                   // /끼임탈출
        public const int Contents_Mvoe = 70000673;                                      // 이동

        public const int TownCharacterShop_Count_EndTouch = 70000674;                   // ({0}/{0}) 터치시 종료
        public const int TownCharacterShop_EndTouch = 70000675;                         // 터치시 종료
        public const int TownCharacterShop_NextTouch = 70000676;                        // {0}/{1}) 터치시 다음
        public const int TownCharacterShop_Count_NextTouch = 70000677;                  // 터치시 다음

        public const int Coustume_HelpNotification_Title = 70000678;                    // 도움말
        public const int Coustume_HelpNotification_Message = 70000679;                  // 전신 코스튬 도움말
        public const int Coustume_HelpNotification_OK = 70000680;                       // 확인

        public const int Developing_Contents_Message = 70000681;                        // [준비 중인 컨텐츠 입니다.]
        public const int Developing_Contents_Message_OK = 70000682;                     // 확인

        public const int PartsCoustume_HelpNotification_Title = 70000683;               // 도움말
        public const int PartsCoustume_HelpNotification_Message = 70000684;             // 전신 코스튬 도움말
        public const int PartsCoustume_HelpNotification_OK = 70000685;                  // 확인

        public const int HeroGrade_4StarMaterial_Confirm = 70000686;                    // 확인
        public const int HeroGrade_4StarMaterial_Cancle = 70000687;                     // 취소
        public const int HeroContentsReport_PopupTitle = 80602;                      // 알림
        public const int HeroContentsReport_PopupReward = 80603;                     // 곤륜 리포트 수집 완료 보상

        public const int SkillSlotInfo_OpenCondition = 70000690;                        // {0}성 달성시 오픈

        public const int DriveEquipPopup_Title = 70000691;                              // 장착중인 {0}번 조각
        public const int DriveInvenPopup_Title = 70000692;                              // 보유중인 {0}번 조각

        public const int HeroGuide_Summon_Title = 70000693;                             // 소환
        public const int HeroGuide_Summon_Text = 70000694;                              // 캐릭터를 소환하시겠습니까?

        public const int ReportExtractionButton_Enable = 80628;                      // 소울데이터 추출하기
        public const int ReportExtractionButton_Disable = 80629;                     // 선택한 혼이 없습니다.
        public const int ReportPopup_TabButton_Have = 70000697;                         // 정보 수집
        public const int ReportPopup_TabButton_NotHave = 70000698;                      // 데이터추출
        public const int ReportState_CannotTake = 70000699;                             // 획득불가
        public const int ReportState_CanTake = 70000700;                                // 획득가능
        public const int ReportState_Take = 70000701;                                   // 보상완료
        public const int Alert_NotEnough_KunlunReportRank = 70000702;                   // {0}랭크 필요
        public const int Alert_NotEnough_SoulData = 70000703;                           // 소울데이터 부족

        public const int HeroSkillInfo_CoolTime = 70000704;                             // {0:F2} 초
        public const int HeroSkillInfo_OpenGrade = 70000705;                            // {0}성 달성시 개방됩니다
        public const int HeroSkillInfo_OpenLevel = 70000706;                            // 캐릭터 {0}레벨이상 개방됩니다.
        public const int HeroSkillInfo_LevelUp = 70000707;                              // 스킬업
        public const int HeroSkillInfo_LevelUp_NotEnoughMaterial = 70000708;            // 재료부족
        public const int HeroSkillInfo_DefaultMessage = 70000709;                       // 자세히 알고 싶은 영웅의 능력을 클릭하세요.
        public const int SkillLevelUpNotify_NotEnough_Level = 70000710;                 // 레벨 부족
        public const int SkillLevelUpNotify_OK = 70000711;                              // 확인
        public const int SkillToolTip_ChainCondition = 70000714;                        // 연계 조건
        public const int SkillToolTip_Time_Sec = 70000715;                              // {0} {1}초

        public const int UIHero_TabName_Team = 80200;                                // 팀 편성
        public const int UIHero_TabName_Info = 80300;                                // 영웅 정보
        public const int UIHero_TabName_Level = 80000;                               // 영웅 레벨
        public const int UIHero_TabName_Grade = 80107;                               // 영웅 등급
        public const int UIHero_TabName_Report = 80600;                              // 곤륜 상황실
        public const int UIHero_TabName_Drive = 80500;                                  // 드라이브
        public const int UIHero_TabName_Skill = 80400;                               // 스킬 레벨업

        public const int UIHero_TopContentsTitle_Team = 80201;                       // 전투에 참여 할 팀을 편성합니다.
        public const int UIHero_TopContentsTitle_Info = 80301;                       // 영웅의 상세정보를 확인합니다.
        public const int UIHero_TopContentsTitle_Level = 80001;                      // 영웅의 레벨을 강화시킵니다.
        public const int UIHero_TopContentsTitle_Grade = 80108;                      // 영웅을 성급시킵니다.
        public const int UIHero_TopContentsTitle_Report = 80601;                     // 영웅의 소울링크를 성장시킵니다.
        public const int UIHero_TopContentsTitle_Drive = 80501;                      // 영웅의 소울드라이브를 관리합니다.
        public const int UIHero_TopContentsTitle_Skill = 80401;                      // 영웅의 스킬을 관리합니다.
        public const int UIHero_TeamBattlePower = 80219;                             // 팀 전투력
        public const int UIHero_BattlePower = 80316;                                 // 전투력
        public const int UIHero_NotUsed_ReaderBuff = 70000731;                          // 리더버프 없음

        public const int InfiniteDungeonCellView_Floor = 71100021;                      // {0:00} 층
        public const int InfiniteDungeonWindow_Floor = 71100022;                        // 봉신의 탑 {0}층

        public const int Inventory_ItemSort_Type = 70000734;                            // 타입순
        public const int Inventory_ItemSort_Time = 70000735;                            // 획득순
        public const int Inventory_NoSelectItem = 70000736;                             // 선택된 아이템이 없습니다.
        public const int Inventory_ListNoData = 70000737;                               // 아이템 정보가 없습니다.
        public const int Inventory_NotConfirm_Item = 70000740;                          // 사용 불가 아이템 입니다.
        public const int Inventory_NoData_Buff = 70000742;                              // 존재 하지 않는 버프 입니다.
        public const int Inventory_BuffUseNotify_Title = 70000743;                      // 버프 적용
        public const int Inventory_BuffUseNotify_Yes = 70000744;                        // 예
        public const int Inventory_BuffUseNotify_No = 70000745;                         // 아니요
        public const int Inventory_GrobalBuffCountCheckText = 2090000112;               //
        public const int Inventory_GrobalBuffGradCheckText = 2090000111;                //
        public const int Inventory_BuffUseNotify_Message = 70000747;                    // 아이템을 사용하시겠습니까?
        public const int Inventory_BuffUse_Success = 70000750;                          // 버프가 적용 되었습니다.

        public const int Inventory_ItemPopupTitle_Use = 70000751;                       // 사용 하기
        public const int Inventory_ItemPopupTitle_Sell = 70000752;                      // 판매 하기

        public const int BookCafe_NoLink = 70000753;                                    // 링크가 없습니다.

        public const int SoulChangeBooth_Time = 70000754;                               // {0}시 {1}분 {2}초
        public const int SoulChangeBooth_AfterTime = 70000755;                          // 0}\n({1} <color=#FF0000>{2} 후</color>

        public const int TripleRaid_NotfiyTitle_Gold = 70000756;                        // 재화 부족
        public const int TripleRaid_NotfiyMessgae_Gold = 70000757;                      // 골드가 부족합니다.
        public const int TripleRaid_NotfiyBtn = 70000758;                               // 확인
        public const int TripleRaid_NotEnoughMember = 70000762;                         // 인원 부족
        public const int TripleRaid_NotEnoughMember_Message = 70000763;                 // 파티의 인원이 부족합니다.\n팀편성 화면으로 이동하시겠습니까?
        public const int TripleRaid_NotfiyBtn_Cancle = 70000765;                        // 취소
        public const int TripleRaid_RewardList = 70000766;                              // 보상목록
        public const int TripleRaid_OpenLevel = 70001187;                               // 계정레벨 {0} 이상\n잠금해제

        public const int Mail_Notify_RemoveTitle = 70000767;                            // 삭제
        public const int Mail_Notify_RemoveMessage = 70000768;                          // 메시지를 삭제 하시겠습니까?
        public const int Mail_Notify_Remove_Ok = 70000769;                              // 예
        public const int Mail_Notify_Remove_Cancle = 70000770;                          // 아니요
        public const int Mail_Type_Info = 70000771;                                     // 안내
        public const int Mail_Type_Notice = 70000772;                                   // 알림
        public const int Mail_Type_Reward = 70000773;                                   // 보상
        public const int Mail_QuestSuccess = 70000774;                                  // 퀘스트 완료 : {0}
        public const int Mail_RemoveTime_HourMin = 70000775;                            // {0}시{1}분 후 메시지삭제
        public const int Mail_RemoveTime_Day = 70000776;                                // {0}일 후 메시지삭제

        public const int NetArenaRank_NoRank = 70000777;                                // 순위\n없음
        public const int NetArenaRank_Rank = 70000778;                                  // {0}위
        public const int NetArenaRank_DefaultRank = 70000780;                           // 훈련인
        public const int NetSpearSkillView_ChainSkill = 70000781;                       // 연계 스킬

        public const int PompomReadyTime = 70000783;                                    // 폼폼 월드 입장까지 남은 시간
        public const int PompomClose = 70000784;                                        // 금일 경기 종료
        public const int PompomOpenTime = 70000785;                                     // 폼폼 월드 신청까지 남은 시간
        public const int PompomPlaying = 70000786;                                      // 경기 진행중

        public const int QuestRewardConfirm = 70000787;                                 // 보상 받기
        public const int QuestSuccess = 70000788;                                       // 완료
        public const int Quest_Contents_Mvoe = 70000789;                                // 이동하기
        public const int QuestConfirm = 70000790;                                       // 받기

        public const int Running_ReMatch = 70000791;                                    // 다시 찾기
        public const int Running_Matching = 70000792;                                   // 검색중….

        public const int SelfShopAttachItem_MaxCount = 70000793;                        // 판매 최대 수량 <color=#ffc600ff>{0}</color> 개
        public const int SelfShopAttachItem_MinPrice = 70000794;                        // 1개 최소 금액 <color=#ffc600ff>{0}</color>
        public const int SelfShopOtherUserName = 70000795;                              // <color=#FCC800FF>{0}</color> 님의 개인 상점
        public const int SelfShopMyName = 70000796;                                     // {0}상점
        public const int UISelfShopLog_UserName = 70000797;                             // <color=#FCC800FF>{0}</color> 님이

        public const int ServerItem_NotUsed = 70000798;                                 // 없음
        public const int ServerItem_Good = 70000799;                                    // 쾌적
        public const int ServerItem_Close = 70000800;                                   // 닫힘

        public const int UIShopExpireItem_Buy = 70000801;                               // 구매
        public const int UIShopNormalItem_Buy = 70000802;                               // 구매
        public const int SnackbarMenuBtn_Ok = 70000803;                                 // 확인

        public const int StoryMode_Stage_NotOpened = 70000804;                          // 오픈되지 않은 스테이지 입니다.
        public const int StoryMode_SweepCount = 70000805;                               // 소탕 {0}회
        public const int StroyMode_NotOpen_Contents = 70000806;                         // 현재 이용할 수 없는 기능입니다.
        public const int StroyMode_Ready_Contents = 70000807;                           // 준비중인 기능입니다.
        public const int StroyMode_3StarClear = 70000808;                               // 스테이지 3성 클리어 후 이용가능합니다.
        public const int StroyMode_DayCount = 70000809;                                 // 일 {0}회 만 이용가능 합니다.
        public const int StoryMode_ClearUse = 70000810;                                 // 스테이지 클리어 후 이용가능합니다.
        public const int StoryMode_NotEnough_Stamina = 70000811;                        // 스테미너 부족

        public const int UIStageSweepTitle_InfiniteDungeon = 71100023;                  // 봉신의 탑 {0}층 결과
        public const int UIStageSweepMultiTitle_InfiniteDungeon = 71100024;             // 봉신의 탑 {0}층 - {1}층 결과
        public const int UIStageSweepTitle_StoryMode = 70000814;                        // 스테이지 {0} - {1} 결과
        public const int StageSweepCellView_Count = 70000815;                           // {0}회

        public const int StarGrdeStep = 70000816;                                       // {0}단계
        public const int HeroTeamContents_NotUsed_LeaderBuff = 70000817;                // 리더버프 없음

        public const int TownMoveMessage_1Zone = 70000818;                              // 1 구역으로 가기
        public const int TownMoveMessage_2Zone = 70000819;                              // 2 구역으로 가기
        public const int TownMoveMessage_3Zone = 70000820;                              // 3 구역으로 가기
        public const int TownRaid_Rank = 70000821;                                      // 피해량 <color=#E8AE0EFF>{0}</color>위
        public const int TownRaid_Time_Min = 70000822;                                  // {0:D2}분
        public const int TownRaid_NotEnounghTicket = 70000824;                          // 티켓이 부족합니다.

        public const int TownRoulette_ItemUse = 70000825;                               // 아이템 룰렛을 사용하시겠습니까?

        public const int TripleRaidBossInfoPopupName = 70000826;                        // {0} 공략
        public const int TripleRaidDifficult_ReadyContents = 70000827;                  // 준비중인 컨텐츠 입니다.
        public const int TripleRaidUserInfo_PartyBanPlayer = 70000828;                  // <color=#31FA31FF>{0}</color>\n파티에서 추방시키겠습니까?
        public const int TripleRaidUserInfo_Exile = 70000829;                           // 추방
        public const int TripleRaidUserInfo_Exile_Ok = 70000830;                        // 확인
        public const int TripleRaidUserInfo_Exile_Cancle = 70000831;                    // 취소

        public const int TeamName_Base = 80202;                                      // 던전 모험팀
        public const int TeamName_Duel = 80203;                                      // 아레나팀
        public const int TeamName_Versus = 80204;                                    // 대전 컨텐츠팀
        public const int TeamName_Challenge = 80205;                                 // 도전 컨첸츠팀
        public const int TeamName_Raid = 80206;                                      // 레이드팀

        public const int SecondToRemainTime_Day = 70000837;                             // {0}일{1}시간 남음
        public const int SecondToRemainTime_Hour = 70000838;                            // {0}시간{1}분 남음
        public const int SecondToRemainTime_Min = 70000839;                             // {0}분 남음
        public const int SecondToRemainTime_OneMin = 70000840;                          // 1분 남음
        public const int SecondToBeforeTime_Day = 70000841;                             // {0}일 전
        public const int SecondToBeforeTime_Hour = 70000842;                            // {0}시간 전
        public const int SecondToBeforeTime_Min = 70000843;                             // {0}분 전
        public const int SecondToBeforeTime_OneMin = 70000844;                          // 1분 이하

        public const int ConvertSecondToTimer_Day = 70000845;                           // {0}일
        public const int ConvertSecondToTimer_Hour = 70000846;                          // {0}시간 {1}분
        public const int ConvertSecondToTimer_Min = 70000847;                           // {0}분 {1}초
        public const int ConvertSecondToTimer_Sec = 70000848;                           // {0}초

        public const int ArenaRemainTime_Day = 70000849;                                // {0}일
        public const int ArenaRemainTime_Hour = 70000850;                               // {0}시간
        public const int ArenaRemainTime_Min = 70000851;                                // {0}분
        public const int ArenaRemainTime_OneMin = 70000852;                             // 1분

        public const int ErrorPopup_Title = 70000853;                                   // 에러({0})
        public const int ErrorPopup_BtnOk = 70000854;                                   // 확인

        public const int NetSpearGrade_Easy = 70000856;                                 // 쉬움
        public const int NetSpearGrade_Normal = 70000857;                               // 보통
        public const int NetSpearGrade_Hard = 70000858;                                 // 어려움
        public const int NetSpearGrade_VeryHard = 70000859;                             // 매우어려움

        public const int WorkShopDisassemble_SelectMaterial = 70000860;                 // 분해할 재료를 선택해주세요.
        public const int WorkShopDisassemble_MaterialCount = 70000861;                  // {0} ~ {1}개
        public const int WorkShopExchange_NotEnoungh_Gold = 70000862;                   // 골드 부족
        public const int WorkShopExchange_NotEnoungh_Material = 70000863;               // 재료 부족
        public const int WorkShopExchange_ReflashCountMax = 70000864;                   // 갱신 횟수를 모두 소진하였습니다.
        public const int WorkShopExchange_ReflashRandomItem = 70000865;                 // 랜덤 상품이 갱신되었습니다.
        public const int WorkShopExchange_SelectItem = 70000867;                        // 교환할 상품을 선택하세요
        public const int WorkShopProducePopup_NotEnoungh_Material = 70000870;               // 재료가 부족합니다.
        public const int WorkShopProducePopup_MaterialChange = 70000871;                     // 재료가 교환되었습니다.


        public const int Battle_FrontLine_AllDeath = 70001398;                                 // 전열이 전멸했다.
        public const int Battle_BackLine_AllDeath = 70001399;                                  // 후열이 전멸했다.
        public const int Battle_WeAreThePower = 70001400;                                      // 우리끼리 힘을 합치자!

        public const int Quest_Reward_CountZero = 70001401;                                    // 보상 가능한 퀘스트가 없습니다.
        public const int TripleRaid_NotFound_ReadyRoom = 70001402;                             // 대기중인 방이 없어졌습니다.
        public const int TripleRaid_Start_Notice = 70001403;                                   // 3인 레이드가 곧 시작됩니다.
        public const int TripleRaid_NotJoinRoom = 70001404;                                    // 입장 불가
        public const int TripleRaid_JoinRoomCountZero = 70001405;                              // 입장할수있는 방이 없습니다.\n방을 생성하시겠습니까?

        public const int PomPom_Event_Close = 70001406;                                        // 폼폼 대전이 종료되었습니다.

        public const int Sever_OnConnectionError = 70001407;                                   // 접속에 실패 했습니다.
        public const int Sever_OnConnectionError_ReConnect = 70001408;                         // 네트워크가 연결되어 있지 않습니다.\n 다시 연결 하시겠습니까?
        public const int Sever_OnConnectedError = 70001409;                                    // 서버와의 접속이 끊어졌습니다.
        public const int ApplicationQuit = 70001410;                                           // 종료 확인
        public const int ApplicationQuitMsg = 70001411;                                        // 게임을 종료하시겠습니까?

        public const int Tutorial_TouchAndDrag = 70001417;                                      // 길게 터치 후 이동 ~~!!

        public const int RunningHero_ItemUse = 70001418;                                               // 아이템 {0}을 사용하였습니다.
        public const int NpcNarba_IdleText = 70001419;                                                 // 여기까지 쫓아오다니....
        public const int Stamina_MoveShop = 70001420;                                                  // 스테미너가 부족합니다 \n 상점으로 이동하시겠습니까?
        public const int Gold_MoveShop = 70001421;                                                     // 재화가 부족합니다 \n 상점으로 이동하시겠습니까?

        /// <summary>
        /// 훈련소
        /// </summary>
        
        public const int Training_NoHavePartyMember = 70001428;                             // 훈련소 진입시 맴버가 1명도 없을경우

        /// <summary>
        /// 친구
        /// </summary>

        public const int Friend = 70001429;                                                            // 친구

        public const int Friend_DeletePopupTitle = 70001430;                                           // 친구 삭제 팝업 타이틀
        public const int Friend_DeletePopupContext = 70001431;                                         // 친구 삭제 팝업 내용
        
        public const int Friend_PopupOK = 70001432;                                                    // 친구 관련 팝업 "확인"

        public const int Friend_Login = 70001433;                                                      // <color=#94F902FF>접속중</color>

        public const int Friend_LoginTime_DayHour = 70001434;                                          // {0}일 {1}시간 전
        public const int Friend_LoginTime_HourMunite = 70001435;                                       // {0}시간 {1}분 전
        public const int Friend_LoginTime_Munite = 70001436;                                           // {0}분 전
        public const int Friend_LoginTime_recently = 70001437;                                         // 최근
        
        public const int Friend_FriendSearch_Condition = 70001438;                                     // 친구 검색 최소 조건
        public const int Friend_FriendSearch_Default = 70001439;                                       // ID를 입력하세요.

        public const int Friend_RefreshTitle = 70001440;                                               // 추천친구 갱신 실패 팝업 타이틀
        public const int Friend_Refresh_Context = 70001441;                                            // 추천친구 갱신 실패 팝업 내용

        public const int Friend_Tab_Recv = 70001442;                                                   // 수락대기
        public const int Friend_Tab_Send = 70001443;                                                   // 요청대기
        public const int Friend_Tab_Recommend = 70001444;                                              // 추천친구
        public const int Friend_Tab_Search = 70001445;                                                 // 친구검색
        public const int Friend_Tab_GameFriend = 70001446;                                             // 게임친구
        public const int Friend_Tab_Accept = 70001447;                                                 // 친구신청

        public const int Friend_SendHornorMax_title = 70001448;                                        // 명예포인트 최대 팝업 타이틀
        public const int Friend_SendHornorMax_context = 70001449;                                      // 명예포인트 최대 팝업 내용
        public const int Friend_SendFriendisFull_title = 70001450;                                     // 신청 인원수 최대 팝업 타이틀
        public const int Friend_SendFriendisFull_context = 70001451;                                   // 신청 인원수 최대 팝업 내용
        public const int Friend_GameFriendisFull_title = 70001452;                                     // 게임 친구가 가득참 팝업 타이틀
        public const int Friend_GameFriendisFull_context = 70001453;                                   // 게임 친구가 가득참 팝업 내용
        public const int Friend_DeleteCountMax_title = 70001454;                                       // 친구 삭제 횟수 초과 팝업 타이틀
        public const int Friend_DeleteCountMax_context = 70001455;                                     // 친구 삭제 횟수 초과 팝업 내용

        public const int Friend_AllSend_title = 70001456;                                                      // 명예포인트 모두 보내기 팝업 결과 타이틀
        public const int Friend_AllSend_context = 70001457;                                                    // 명예포인트 {0} 을 획득하였습니다.
        public const int Friend_SendHornor_title = 70001458;                                                   // 1인 명예포인트 결과 팝업 타이틀
        public const int Friend_SendHornor_context = 70001459;                                                 // 1인 명예포인트 결과 팝업 내용
        public const int Friend_Cannot_SendHonorAll_title = 70001460;                                           // 명예 모두보내기 - 보낼수 있는 친구가 없음 타이틀
        public const int Friend_Cannot_SendHonorAll_context = 70001461;                                         // 명예 모두보내기 - 보낼수 있는 친구가 없음 내용
        public const int Friend_Search_FindMe_title = 70001471;                                                      // 나를 검색했을 경우 팝업 타이틀
        public const int Friend_Search_FindMe_context = 70001472;                                                      // 나를 검색했을 경우 팝업 타이틀
        public const int Friend_Search_FindRecv_title = 70001473;                                                      // 나를 검색했을 경우 팝업 타이틀
        public const int Friend_Search_FindRecv_context = 70001474;                                                      // 나를 검색했을 경우 팝업 타이틀
        public const int Friend_Search_FindSend_title = 70001475;                                                      // 나를 검색했을 경우 팝업 타이틀
        public const int Friend_Search_FindSend_context = 70001476;                                                      // 나를 검색했을 경우 팝업 타이틀
        public const int Friend_Search_FindGame_title = 70001477;                                                      // 나를 검색했을 경우 팝업 타이틀
        public const int Friend_Search_FindGame_context = 70001478;                                                      // 나를 검색했을 경우 팝업 타이틀
        public const int Friend_Search_NotNickName = 70001477;                                                 // 존재하지 않는 유저를 검색했을 경우

        public const int Friend_AcceptFail_Title = 70001479;                                                   // 타운에서 친구신청 할떄 뜨는 팝업 타이틀
        public const int Friend_AcceptFail_SendList = 70001480;                                                // 타운에서 친구신청 할떄 뜨는 팝업 내용
        public const int Friend_AcceptFail_RecvList = 70001481;
        public const int Friend_AcceptFail_GameList = 70001482;

        public const int Friend_Teleport_SameZone_title = 70001540;
        public const int Friend_Teleport_SameZone_context = 70001541;

        /// <summary>
        /// 게임상점 텍스트
        /// </summary>
        public const int GameShop_Title_Shop = 70001148;
        public const int GameShop_Title_EventShop = 10110;                                              // 이벤트 상점
        public const int GameShop_Title_RollShop = 10111;                                               // 소환 상점
        public const int GameShop_Title_NormalShop = 10112;                                             // 일반 상점
        public const int GameShop_Title_CostumeShop = 10113;                                            // 코스튬 상점
        public const int GameShop_Title_FishingShop = 10114;                                            // 낚시 상점
        public const int GameShop_Title_DrillShop = 10115;                                              // 드릴 상점
        public const int GameShop_Title_TownAction = 10116;                                              // 드릴 상점
        
        public const int GameShop_BuyFail_Title_NotEnoughConsume = 10040;
        public const int GameShop_BuyFail_Context_NotEnoughConsume = 10041;
        
        public const int GameShop_BuyPopup_Title = 10145;                                               // 상점 아이템 구매 - 팝업 타이틀
        public const int GameShop_BuyPopup_ContextTitle = 10146;                                    // 상점 아이템 구매 - 팝업 내용 타이틀
        public const int GameShop_BuyPopup_Context = 10147;                                         // 상점 아이템 구매 - 팝업 내용
        public const int GameShop_BuyPopup_Button = 10148;                                              // 구매하기

        /// <summary>
        /// 코스튬 상점 텍스트
        /// </summary>
        public const int CostumeShop_BasicName_All = 10021;                     // 기본의상
        public const int CostumeShop_BasicName_Weapon = 10022;                  // 기본무기
        public const int CostumeShop_BasicName_Head = 10023;                    // 기본머리

        /// <summary>
        /// 캐릭터 SubCamp 텍스트
        /// </summary>
        public const int HeroGuide_SubCamp_Asoetic = 0;                                   // 신선
        public const int HeroGuide_SubCamp_Helper = 0;                                    // 조력자
        public const int HeroGuide_SubCamp_ThirtySix_Chungangc = 0;                       // 36천강
        public const int HeroGuide_SubCamp_SeventyTwo_Jisal = 0;                          // 72지살
        public const int HeroGuide_SubCamp_Suho = 0;                                      // 수호지군
        public const int HeroGuide_SubCamp_Ten_Chungun = 0;                               // 십천군
        public const int HeroGuide_SubCamp_Twelve_Jingun_Human = 0;                       // 십이진군(인간)
        public const int HeroGuide_SubCamp_Twelve_Jingun_Monster = 0;                     // 십이진군(변신)
        public const int HeroGuide_SubCamp_Normal_Monser = 0;                             // 일반 요괴
        public const int HeroGuide_SubCamp_Great_Monster = 0;                             // 8대 요괴

    }
}
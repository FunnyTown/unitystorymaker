﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace StoryMaker
{
    [System.Serializable]
    public class StepInfo
    {
        public string StepName;
        public PlayState PlayState = PlayState.wait;

        [HideInInspector] public float Duration;
        [HideInInspector] public string EndString;
        [HideInInspector] public int EndStringID;

        public GameObject BallonObj;
        public Image TextBallonImageObj;
        public Text TextObj;
        public GameObject TouchObj;

        [HideInInspector] public bool IsUseOtherImage { get; set; }
        public List<DirectionInfo> OtherDirectionInfos = new List<DirectionInfo>();

        [HideInInspector] public Action StepEndFnc;
        [HideInInspector] public StepInfoObject TemplateObject;

        [HideInInspector] public bool IsAutoResize;

        public void Initialize()
        {
            FindObj();
            if (null != TextBallonImageObj)
                TextBallonImageObj.gameObject.SetActive(false);

            if (null != TextObj)
            {
                TextObj.gameObject.SetActive(false);
                TextObj.text = string.Empty;
            }

            if (null != TouchObj)
            {
                TouchObj.gameObject.SetActive(false);
            }

            foreach (DirectionInfo info in OtherDirectionInfos)
            {
                if (null != info.TargetObj)
                    info.TargetObj.gameObject.SetActive(false);
            }
        }

        private void FindObj()
        {
            if (null == BallonObj)
                return;

            TextBallonImageObj = BallonObj.transform.Find("ImageBallon").GetComponent<Image>();
            TextObj = BallonObj.transform.Find("TextString").GetComponent<Text>();

            Transform findTouchImage = BallonObj.transform.Find("ImageTouch");
            if(null == findTouchImage)
            {
                findTouchImage = TextBallonImageObj.transform.Find("ImageTouch");
                if (null != findTouchImage)
                    TouchObj = findTouchImage.gameObject;
            }
            else
            {
                TouchObj = BallonObj.transform.Find("ImageTouch").gameObject;
            }
        }
    }
}

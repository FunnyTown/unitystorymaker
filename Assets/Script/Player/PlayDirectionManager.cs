﻿using DG.Tweening;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace StoryMaker
{

    public class PlayDirectionManager
    {
        public StepInfo CurrentInfo;

        public bool otherinfoComplete = true;
        public bool stepinfoComplete = true;

        public bool SkipDirection = false;
        public bool SkipStep = false;
        public bool EndAnotherDirection = false;

        /// <summary>
        /// 동시에 진행하는 유아이들 리스트
        /// </summary>
        private List<DirectionInfo> _multiAniObj = new List<DirectionInfo>();

        /// <summary>
        /// 스탭이 끝나고 난 뒤 그라데이션을 깔아줄 오브젝트 리스트
        /// </summary>
        private List<GameObject> _gradationObj = new List<GameObject>();

        /// <summary>
        /// 스탭이 끝나고 자동으로 색상을 돌려주는 오브젝트 리스트
        /// </summary>
        private List<GameObject> _colorWhiteObj = new List<GameObject>();

        /// <summary>
        /// 스탭이 끝나고 자동으로 비활성화 처리하는 오브젝트 리스트
        /// </summary>
        private List<GameObject> _activeFalseObj = new List<GameObject>();

        private Color _gradationColor = Color.gray;

        //private Color _touchImageColor = new Color(0, 244, 255);
        
        public IEnumerator ShowDirection(DirectionInfo otherinfo)
        {
            /// 자동화 부분 관련
            if (true == otherinfo.IsAutoActiveFalue)
                _activeFalseObj.Add(otherinfo.TargetObj);

            switch (otherinfo.DirectionType)
            {
                case DirectionType.ChangeColor:
                    {
                        ShowDirection_ChangeColor(otherinfo);
                    }
                    break;
                case DirectionType.Move:
                    {
                        ShowDirection_Move(otherinfo);
                    }
                    break;
                case DirectionType.Jump:
                    {
                        ShowDirection_Jump(otherinfo);
                    }
                    break;
                case DirectionType.Shake:
                    {
                        ShowDirection_Shake(otherinfo);
                    }
                    break;
                case DirectionType.Bounce:
                    {
                        ShowDirection_Bounce(otherinfo);
                    }
                    break;
                case DirectionType.Swap:
                    {
                        ShowDirection_Swap(otherinfo);
                    }
                    break;
                case DirectionType.Active:
                    {
                        ShowDirection_Active(otherinfo);
                    }
                    break;
                case DirectionType.Rotation:
                    {
                        ShowDirection_Rotation(otherinfo);
                    }
                    break;
                case DirectionType.Scale:
                    {
                        ShowDirection_Scale(otherinfo);
                    }
                    break;
                case DirectionType.FadeInOut:
                    {
                        ShowDirection_FadeInOut(otherinfo);
                    }
                    break;
                case DirectionType.Enable:
                    {
                        ShowDirection_Enable(otherinfo);
                    }
                    break;
                default:
                    break;
            }

            yield return null;
        }

        /// <summary>
        /// 연출 시작할때 실행해주는 부분
        /// </summary>
        /// isOther = TextAni 연출이 아닌 다른 이미지 연출일 경우 사용
        public void StartDirection(bool isOther)
        {
            if (isOther)
                otherinfoComplete = false;
            else
                stepinfoComplete = false;
        }

        /// <summary>
        /// 스탭 연출 완료
        /// </summary>
        public void DirectionComplete()
        {
            stepinfoComplete = true;
            SkipDirection = false;
            SkipStep = false;
            EndAnotherDirection = false;

            SetAutoList();
            
            CurrentInfo.PlayState = PlayState.complete;
            
            if(null != CurrentInfo.TextBallonImageObj)
                CurrentInfo.TextBallonImageObj.gameObject.SetActive(false);

            if(null != CurrentInfo.TouchObj)
                CurrentInfo.TouchObj.gameObject.SetActive(false);

            if (null != CurrentInfo.TextObj)
                CurrentInfo.TextObj.gameObject.SetActive(false);
        }
        
        /// <summary>
        /// 스탭 연출 외에 다른 연출 완료
        /// </summary>
        public void OtherInfoComplete()
        {
            MultiAniListComplete();

            otherinfoComplete = true;
            SkipDirection = false;
            SkipStep = false;
        }

        /// <summary>
        /// 리소스 활성화
        /// </summary>
        private void ActiveObject(GameObject target, bool active)
        {
            if (null == target)
                return;

            target.SetActive(active);
        }

        /// <summary>
        /// 현재 진행중인 연출 리스트
        /// </summary>
        private void SetAniList(DirectionInfo addInfo)
        {
            if (null == addInfo)
                return;

            _multiAniObj.Add(addInfo);
        }

        private void MultiAniListComplete()
        {
            foreach (DirectionInfo resource in _multiAniObj)
            {
                resource.CompletAni();
            }

            _multiAniObj.Clear();
        }

        /// 메인 연출 기능
        #region Main Direction

        /// <summary>
        /// 오브젝트 비활성화
        /// </summary>
        public IEnumerator ShowDirection_ActiveFalse(GameObject resource, bool isOtherInfo)
        {
            ActiveObject(resource, false);

            if (true == isOtherInfo)
                OtherInfoComplete();

            yield return null;
        }

        /// <summary>
        /// 오브젝트 활성화
        /// </summary>
        public IEnumerator ShowDirection_ActiveTrue(GameObject resource, bool isOtherInfo)
        {
            ActiveObject(resource, true);

            if (true == isOtherInfo)
                OtherInfoComplete();

            yield return null;
        }

        /// <summary>
        /// Text 하나씩 출력해주는 연출
        /// </summary>
        public IEnumerator ShowDirection_StringAni()
        {
            if (null == CurrentInfo)
            {
                DirectionComplete();
                yield return null;
            }

            Text ballonText = CurrentInfo.TextObj;
            string endString = CurrentInfo.EndString;
            float duration = CurrentInfo.Duration;
            int textCount = 0;

            ballonText.text = string.Empty;

            ballonText.DOText(endString, duration)
                .OnStart(() =>
                {
                    ActiveObject(CurrentInfo.TextBallonImageObj.gameObject, true);
                    ActiveObject(CurrentInfo.TextObj.gameObject, true);

                    StartDirection(false);
                })
                .OnComplete(() =>
                {
                    //ShowDirection_StepTouchBlue();
                    ShowDirection_TouchImageActive();
                })
                .OnUpdate(() =>
                {
                    if (SkipDirection == true)
                    {
                        ballonText.DOKill();
                        ballonText.text = endString;

                        SkipDirection = false;
                        //ShowDirection_StepTouchBlue();
                        ShowDirection_TouchImageActive();
                    }
                    if(ballonText.text.Length > textCount)
                    {
                        textCount = ballonText.text.Length;
                    }
                });

            yield return null;
        }

        public void ShowDirection_TouchImageActive()
        {
            EndAnotherDirection = true;
            CurrentInfo.TouchObj.gameObject.SetActive(true);
        }

        /// <summary>
        /// 터치 이미지 색상 트윈 연출
        /// </summary>
        public void ShowDirection_StepTouchBlue()
        {
            //CurrentInfo.TouchImageObj.gameObject.SetActive(true);
            //CurrentInfo.TouchImageObj.DOColor(_touchImageColor, 0.5f)
            //   .OnStart(() =>
            //   {
            //       CurrentInfo.TouchImageObj.gameObject.SetActive(true);
            //       EndAnotherDirection = true;
            //   })
            //   .OnComplete(() =>
            //   {
            //       CurrentInfo.TouchImageObj.DOColor(Color.white, 0.5f)
            //       .OnComplete(() =>
            //       {
            //           ShowDirection_StepTouchBlue();
            //       });
            //   })
            //   .OnUpdate(() =>
            //   {
            //       if (SkipDirection == true)
            //       {
            //           CurrentInfo.TouchImageObj.DOKill();
            //           CurrentInfo.TouchImageObj.color = new Color(255, 255, 255);
            //           CurrentInfo.TouchImageObj.gameObject.SetActive(false);

            //           DirectionComplete();
            //       }
            //   });
        }

        /// <summary>
        /// 색상 변경
        /// </summary>
        public void ShowDirection_ChangeColor(DirectionInfo otherInfo)
        {
            if(null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.ChangeColor(Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }

        /// <summary>
        /// Move 연출
        /// </summary>
        public void ShowDirection_Move(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.Move(otherInfo.TargetObj.transform.GetComponent<RectTransform>(), Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }
        
        /// <summary>
        /// 스왑
        /// TODO jaewoo - 수정해야함
        /// </summary>
        public void ShowDirection_Swap(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.Swap(Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }

        /// <summary>
        /// 타겟 오브젝트 활성화 / 비활성화
        /// </summary>
        public void ShowDirection_Active(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            if(otherInfo.ActiveType == ActiveType.True)
                otherInfo.TargetObj.SetActive(true);
            else
                otherInfo.TargetObj.gameObject.SetActive(false);

            Direction_OnComplte();
        }

        /// <summary>
        /// 캐릭터 쉐이크
        /// </summary>
        public void ShowDirection_Shake(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.Shake(otherInfo.TargetObj.transform.GetComponent<RectTransform>(), Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }

        /// <summary>
        /// 점프
        /// </summary>
        public void ShowDirection_Bounce(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.Bounce(otherInfo.TargetObj.transform.GetComponent<RectTransform>(), Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }
        
        /// <summary>
        /// 폴짝 뛰는 모션
        /// </summary>
        public void ShowDirection_Jump(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.Jump(otherInfo.TargetObj.transform.GetComponent<RectTransform>(), Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }
        
        /// <summary>
        /// 회전 연출
        /// </summary>
        public void ShowDirection_Rotation(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.Rotation(otherInfo.TargetObj.transform.GetComponent<RectTransform>(), Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }

        public void ShowDirection_Scale(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.Scale(otherInfo.TargetObj.transform.GetComponent<RectTransform>(), Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }

        public void ShowDirection_FadeInOut(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.FadeInOut(otherInfo.TargetObj.transform.GetComponent<RectTransform>(), Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }
        
        public void ShowDirection_Enable(DirectionInfo otherInfo)
        {
            if (null == otherInfo)
            {
                OtherInfoComplete();
            }

            otherInfo.Enable(otherInfo.TargetObj.transform.GetComponent<RectTransform>(), Direction_OnStart, Direction_OnComplte, Direction_OnUpdate);
        }

        #endregion

        /// 기본 연출 기능 ( 스타트, 업데이트, 컴플리트 )
        #region Base Direction ( Start, Update, Complete )
        /// <summary>
        /// 연출 스타트
        /// </summary>
        public void Direction_OnStart(DirectionInfo getInfo)
        {
            ActiveObject(getInfo.TargetObj, true);

            if (getInfo.IsMultiPlay == false)
                StartDirection(true);

            SetAniList(getInfo);
        }

        /// <summary>
        /// 연출 완료
        /// </summary>
        public void Direction_OnComplte()
        {
            OtherInfoComplete();
        }

        /// <summary>
        /// 연출 업데이트
        /// </summary>
        /// <param name="getInfo"></param>
        public void Direction_OnUpdate(DirectionInfo getInfo)
        {
            if (SkipDirection == true)
            {
                getInfo.CompletAni();

                OtherInfoComplete();
            }
        }
        #endregion

        /// 자동 색상 변경 부분
        #region Auto Gradation

        public void SetAutoList()
        {
            //SetGradationList();
            //SetColorList();
            SetActiveFalseList();
        }

        /// <summary>
        /// 스탭이 끝나고 이미지 그라데이션 넣어줌
        /// </summary>
        public void SetGradationList()
        {
            if (0 == _gradationObj.Count)
                return;

            Image findImage;
            Text findText;

            foreach (GameObject resource in _gradationObj)
            {
                findImage = resource.GetComponent<Image>();
                if(null != findImage)
                {
                    findImage.DOColor(_gradationColor, 0.2f);
                    continue;
                }

                findText = resource.GetComponent<Text>();
                if (null != findText)
                {
                    findText.DOColor(_gradationColor, 0.2f);
                    continue;
                }
            }

            _gradationObj.Clear();
        }

        /// <summary>
        /// 이전에 바뀐 색상을 원상태( white ) 로 돌려줌
        /// </summary>
        public void SetColorList()
        {
            if (0 == _colorWhiteObj.Count)
                return;

            Image findImage;
            Text findText;

            foreach (GameObject listItem in _colorWhiteObj)
            {
                findImage = listItem.GetComponent<Image>();
                if (null != findImage && findImage.color.a != 0)
                {
                    findImage.color = Color.white;
                    return;
                }

                findText = listItem.GetComponent<Text>();
                if (null != findText && findText.color.a != 0)
                {
                    findText.color = Color.white;
                    return;
                }
            }
        }

        public void SetActiveFalseList()
        {
            if (0 == _activeFalseObj.Count)
                return;

            foreach (GameObject listItem in _activeFalseObj)
            {
                listItem.SetActive(false);
            }
        }

        #endregion
    }
}

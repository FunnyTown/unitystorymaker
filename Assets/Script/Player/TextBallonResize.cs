﻿
//#define TESTMODE

using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TouchImagePosition
{
    private int WIDTH_BY_CHAR1 = 10;
    private int HEIGHT_BY_CHAR1 = 10;

    private int XsizeHarp = 10;

    public float GetTouchPosX(int charWidthCount)
    {
        return (WIDTH_BY_CHAR1 * charWidthCount) + XsizeHarp;
    }

    public float GetTouchPosY(int charHeightCount)
    {
        charHeightCount--;

        return HEIGHT_BY_CHAR1 * charHeightCount;
    }
}

public class TextBallonResize : MonoBehaviour {

    //TouchImagePosition TouchImagePos;

    public string TestString;

    private const int WIDTH_MIN_SIZE = 222;
    private const int HEIGHT_MIN_SIZE = 138;

    private const int WIDTH_CHAR1 = 25;
    private const int HEIGHT_ENTER1 = 40;

    private bool _isFindResource = false;

    private Image _imageBallon;
    private Text _text;
    private RectTransform _touchRect;
#if TESTMODE
    private Button button;
#endif
    int MaxWidth;
    int MaxHeight;
    
    public void Awake()
    {
        //TouchImagePos = new TouchImagePosition();
        FindObject();
    }

    public void FindObject()
    {
        if (false == _isFindResource)
        {
            _imageBallon = gameObject.GetComponent<Image>("ImageBallon");
            _text = gameObject.GetComponent<Text>("TextString");
            _touchRect = gameObject.GetComponent<RectTransform>("ImageTouch");

#if TESTMODE
            gameObject.AddComponent<Button>();
            button = GetComponent<Button>();
            button.onClick.AddListener(TestFnc);
            
#endif
            _isFindResource = true;
        }
    }

    public void ResetMaxSize()
    {
        MaxWidth = 0;
        MaxHeight = 0;
    }

#if TESTMODE
    public void TestFnc()
    {
        SetTextMaxSize(_text.text);
    }
#endif

    public void SetTextMaxSize(string showString)
    {
        if(false == _isFindResource)
            FindObject();

        int enterIndex;
        string makingText = showString;
        
        ResetMaxSize();

        while (makingText.Length != 0)
        {
            MaxHeight++;
            enterIndex = makingText.IndexOf('\n');
            if(enterIndex <= 0)
            {
                SetWidthSize(makingText.Length);
                makingText = "";
            }
            else
            {
                int nowLength = makingText.Length;
                SetWidthSize(enterIndex);
                makingText = makingText.Substring(enterIndex+1, nowLength - enterIndex-1);
            }
        }

        SetFontSize();
        SetTouchImageSize();

        _imageBallon.rectTransform.SetWidth(GetImageWitdh());
        _imageBallon.rectTransform.SetHeight(GetImageHeight());

#if TESTMODE
        _text.text = "";
        _text.DOText(showString, 2);
#endif

        //_touchRect.anchoredPosition = new Vector2(TouchImagePos.GetTouchPosX(MaxWidth), -1 * TouchImagePos.GetTouchPosY(MaxHeight));
    }

    void SetFontSize()
    {
        _text.fontSize = 23;
    }

    void SetTouchImageSize()
    {
        _touchRect.transform.localScale = new Vector3(0.09f, 0.09f, 0.09f);
    }

    public float GetImageWitdh()
    {
        if (MaxWidth * WIDTH_CHAR1 < WIDTH_MIN_SIZE)
            return WIDTH_MIN_SIZE;
        else
            return MaxWidth * WIDTH_CHAR1;
    }

    public float GetImageHeight()
    {
        if (MaxHeight * HEIGHT_ENTER1 < HEIGHT_MIN_SIZE)
            return HEIGHT_MIN_SIZE;
        else
            return MaxHeight * HEIGHT_ENTER1;
    }

    public void SetWidthSize(int index)
    {
        if (MaxWidth < index)
            MaxWidth = index;
    }

    public int CheckingSpecialText(string txt)
    {
        int count;
        string str = @"[~!@\#$%^&*\()\=+|\\/:;?""<>']";
        System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(str);
        count = int.Parse(rex.Matches(txt, 0).Count.ToString());

        return count;
    }
}

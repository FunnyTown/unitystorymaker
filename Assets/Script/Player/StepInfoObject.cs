﻿using StoryMaker;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoryMaker
{
    [System.Serializable]
    public class StepInfoObject : MonoBehaviour
    {
        public StepInfo stepinfo = new StepInfo();
        
        public void SetStepInfo(StepInfo mStepinfo)
        {
            stepinfo = mStepinfo;
        }

        public StepInfo GetStepInfo()
        {
            return stepinfo;
        }

        public void SetName(string name)
        {
            gameObject.name = name;
        }

        public void SetStepInfos()
        {
            DirectionInfoObject[] directionObjects = gameObject.GetComponentsInChildren<DirectionInfoObject>();

            stepinfo.OtherDirectionInfos.Clear();
            for (int index = 0; index < directionObjects.Length; index++)
            {
                stepinfo.OtherDirectionInfos.Add(directionObjects[index].GetDirectionInfo());
            }
        }
    }
}



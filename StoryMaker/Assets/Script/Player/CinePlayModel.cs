﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace StoryMaker
{
    /// <summary>
    /// 액터 타입
    /// </summary>
    public enum ActorType
    {
        NONE,
        Image,
        Text
    }

    /// <summary>
    /// 스탭의 플레이 상태
    /// </summary>
    public enum PlayState
    {
        wait,
        playing,
        complete
    }
    
    /// <summary>
    /// 연출 타입
    /// </summary>
    public enum DirectionType
    {
        Move = 0,
        ChangeColor = 1,
        Jump = 2,
        Shake = 3,
        Bounce = 4,
        Swap = 5,
        Active = 6,
        SpineAni = 7,
        Rotation = 8,
        Scale = 9,
        FadeInOut = 10,
        Enable = 11
    }

    /*
     * Enable,
        Enable_TextBallon,
        Disable,
        Disable_TextBallon,
        Move,
        ChangeColor,
        ChangeColor_TextBallon,
        Jump,
        Shake,
        Bounce,
        Swap,
        Active,
        SpineAni,
        Rotation,
        Scale,
        FadeInOut
     * */

    public enum StepOptionType
    {
        Connect,
        Create,
        Save
    }

    public enum RecourceType
    {
        TextBallon,
        Image,
        Text,
        Spine
    }

    public enum DetailOptionType
    {
        Save
    }

    public enum ActiveType
    {
        True,
        False
    }

    public enum FadeInOutType
    {
        In,
        Out
    }

    public enum EnableType
    {
        True,
        False
    }

    [System.Serializable]
    public class DirectionInfo
    {
        public DirectionType DirectionType = DirectionType.Enable;

        // 연출 타겟
        public GameObject TargetObj;
        // 연출 시간
        public float Duration;
        // 시작위치
        public Vector2 StartPos;
        // 종료위치
        public Vector2 EndPos;
        // 색상 연출 값
        public Color Color;
        // 동시연출 인지 체크
        public bool IsMultiPlay;
        // 자동 그라데이션을 줄건지
        public bool IsGradationStepEnd;
        // 자동으로 꺼줄지
        public bool IsAutoActiveFalue;
        // 자동으로 색상을 흰색으로 바꿔줄지
        public bool IsAutoColorWhite;
        // 활성화 타입 ( 활성화, 비활성화 )
        public ActiveType ActiveType = ActiveType.True;
        // 설명
        public string Discription = "";

        ////////////////////////////////////////////////////////////
        // 스왑 관련
        // 스왑 타겟 이미지
        public Sprite SwapImage;
        // 스왑 오브젝트
        public GameObject SwapObj;
        ////////////////////////////////////////////////////////////

        // 스왑, 점프 에서 애니메이션을 스킵할 경우 사용하는 기존 위치
        private Vector2 _orizonPos;
        // 점프 높이
        public float JumpHeight;
        // 바운싱 횟수
        public int BounceCount;
        // 로테이션
        public Vector3 EndRotation;
        // 스케일
        public Vector3 EndScale;

        public GameObject EnableStepInfo;
        public GameObject DisableStepInfo;

        // 애니메이션 종료후 들어오는 이벤트
        private Action SpinAniCompleteAction;
        // 페이드 인 / 아웃 
        public FadeInOutType FadeType;
        // 페이드인 / 아웃을 쓸것인지 말것인지
        public bool IsFreeze;

        // 말풍선 연출 자동 리사이징 여부
        public bool IsAutoResize;
        // 활성, 비활성 관련
        public EnableType EnableType;

        public void Initialize()
        {
            if (null != TargetObj)
                TargetObj.SetActive(true);
        }

        public void ChangeColor(Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {
            Image findImage = TargetObj.GetComponent<Image>();
            if(null != findImage)
            {
                findImage.DOColor(Color, Duration)
               .OnStart(() => onStart(this))
               .OnComplete(() =>
               {
                    findImage.DOKill();
                    onComplete();
               })
               .OnUpdate(() => onUpdate(this));
                return;
            }

            Text findText = TargetObj.GetComponent<Text>();
            if (null != findText)
            {
                findText.DOColor(Color, Duration)
               .OnStart(() => onStart(this))
               .OnComplete(() => onComplete())
               .OnUpdate(() => onUpdate(this));
                return;
            }
        }

        public void Move(RectTransform rectTrnasform, Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {
            TargetObj.transform.GetComponent<RectTransform>().anchoredPosition = StartPos;
            rectTrnasform.DOAnchorPos(EndPos, Duration)
               .OnStart(() => onStart(this))
               .OnComplete(() => onComplete())
               .OnUpdate(() => onUpdate(this));
        }
        
        public void Shake(RectTransform rectTrnasform, Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {
            _orizonPos = rectTrnasform.anchoredPosition;
            rectTrnasform.DOShakeAnchorPos(Duration, 50, 100, 0)
               .OnStart(() => onStart(this))
               .OnComplete(() => onComplete())
               .OnUpdate(() => onUpdate(this));
        }

        public void Bounce(RectTransform rectTrnasform, Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {
            TargetObj.transform.GetComponent<RectTransform>().anchoredPosition = StartPos;
            rectTrnasform.DOJumpAnchorPos(EndPos, 100, BounceCount, Duration)
               .OnStart(() => onStart(this))
               .OnComplete(() => onComplete())
               .OnUpdate(() => onUpdate(this));
        }

        public void Jump(RectTransform rectTrnasform, Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {            
            _orizonPos = rectTrnasform.anchoredPosition;
            Vector2 endValue = new Vector2(_orizonPos.x, _orizonPos.y + JumpHeight);

            rectTrnasform.DOJumpAnchorPos(endValue, 30, 1, Duration)
               .OnStart(() => onStart(this))
               .OnComplete(() => onComplete())
               .OnUpdate(() => onUpdate(this));
        }

        public void Swap(Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {
            onStart(this);
            Image findImage = TargetObj.GetComponent<Image>();
            if (null != findImage)
                findImage.sprite = SwapImage;

            onComplete();
        }
        
        public void Rotation(RectTransform rectTrnasform, Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {
            rectTrnasform.DORotate(EndRotation, Duration)
               .OnStart(() => onStart(this))
               .OnComplete(() => onComplete())
               .OnUpdate(() => onUpdate(this));
        }

        public void Scale(RectTransform rectTrnasform, Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {
            rectTrnasform.DOScale(EndScale, Duration)
               .OnStart(() => onStart(this))
               .OnComplete(() => onComplete())
               .OnUpdate(() => onUpdate(this));
        }

        public void FadeInOut(RectTransform rectTrnasform, Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {
            Color fadeInColor = new Color(1, 1, 1, 0);
            Color fadeOutColor = new Color(1, 1, 1, 1);
            Color FadeColor;
            Color FirstColor;

            if (FadeType == FadeInOutType.In)
            {
                FadeColor = fadeInColor;
                FirstColor = fadeOutColor;
            }
            else
            {
                FadeColor = fadeOutColor;
                FirstColor = fadeInColor;
            }
                
            Image findImage = TargetObj.GetComponent<Image>();
            if (null != findImage)
            {
                findImage.color = FirstColor;

                findImage.DOColor(FadeColor, Duration)
                .OnStart(() => 
                {
                    onStart(this);
                })
                .OnComplete(() =>
                {
                    findImage.DOKill();
                    findImage.color = FadeColor;
                    onComplete();
                })
                .OnUpdate(() => onUpdate(this));
                return;
            }

            //Text findText = TargetObj.GetComponent<Text>();
            //if (null != findText)
            //{
            //    findText.DOColor(FadeColor, Duration)
            //   .OnStart(() => onStart(this))
            //   .OnComplete(() => onComplete())
            //   .OnUpdate(() => onUpdate(this));
            //    return;
            //}

            //SkeletonGraphic findSpine = TargetObj.GetComponent<SkeletonGraphic>();
            //if (null != findSpine)
            //{
            //    findSpine.DOColor(FadeColor, Duration)
            //   .OnStart(() => onStart(this))
            //   .OnComplete(() => onComplete())
            //   .OnUpdate(() => onUpdate(this));
            //    return;
            //}
        }

        public void Enable(RectTransform rectTrnasform, Action<DirectionInfo> onStart, Action onComplete, Action<DirectionInfo> onUpdate)
        {
            Color changeColor;
            if (EnableType.True == EnableType)
                changeColor = Color.white;
            else
                changeColor = Color.gray;

            Image findImage = TargetObj.GetComponent<Image>();
            if (null != findImage)
            {
                findImage.DOColor(changeColor, Duration)
               .OnStart(() => onStart(this))
               .OnComplete(() =>
               {
                   findImage.DOKill();
                   onComplete();
               })
               .OnUpdate(() => onUpdate(this));
                return;
            }

            Text findText = TargetObj.GetComponent<Text>();
            if (null != findText)
            {
                findText.DOColor(changeColor, Duration)
               .OnStart(() => onStart(this))
               .OnComplete(() => onComplete())
               .OnUpdate(() => onUpdate(this));
                return;
            }
        }

        public void CompletAni()
        {
            switch(DirectionType)
            {
                case DirectionType.ChangeColor:
                    {
                        Image findImage = TargetObj.GetComponent<Image>();
                        if (null != findImage)
                        {
                            findImage.DOKill();
                        }
                    }
                    break;
                case DirectionType.Move:
                    {
                        TargetObj.transform.GetComponent<RectTransform>().DOKill();
                        TargetObj.transform.GetComponent<RectTransform>().anchoredPosition = EndPos;
                    }
                    break;
                case DirectionType.Jump:
                case DirectionType.Shake:
                    {
                        TargetObj.transform.GetComponent<RectTransform>().DOKill();
                        TargetObj.transform.GetComponent<RectTransform>().anchoredPosition = _orizonPos;
                    }
                    break;
                case DirectionType.Bounce:
                    {
                        TargetObj.transform.GetComponent<RectTransform>().anchoredPosition = EndPos;
                    }
                    break;
                case DirectionType.FadeInOut:
                    {
                        Color fadeInColor = new Color(1, 1, 1, 0);
                        Color fadeOutColor = new Color(1, 1, 1, 1);
                        Color FadeColor;

                        if (FadeType == FadeInOutType.In)
                        {
                            FadeColor = fadeInColor;
                        }
                        else
                        {
                            FadeColor = fadeOutColor;
                        }

                        Image findImage = TargetObj.GetComponent<Image>();
                        if (null != findImage)
                        {
                            findImage.DOKill();
                            findImage.color = FadeColor;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace StoryMaker
{
    public class StoryProp : MonoBehaviour
    {
        [HideInInspector] public PlayState PlayState = PlayState.playing;
        private PlayDirectionManager DirectionPlayer;

        [HideInInspector] public List<StepInfo> StepList = new List<StepInfo>();
        
        [HideInInspector] public StepInfo CurrentStep;

        /// <summary>
        /// 현재 시나리오 연출이 다 완료된 후 들어오는 Event
        /// </summary>
        public Action CinemaEndEventFnc { get; set; }

        private bool _isInitialize = false;
        public void Initialize()
        {
            DirectionPlayer = new PlayDirectionManager();
            Button nextButton = transform.Find("NextButton").GetComponent<Button>();
            if (null != nextButton)
            {
                nextButton.onClick.AddListener(OnClick_StepComplete);
            }

            foreach (StepInfo item in StepList)
            {
                item.Initialize();
                item.StepEndFnc = OnClick_StepComplete;
            }

            _isInitialize = true;
        }

        public void OnClick_StepComplete()
        {
            if(true == DirectionPlayer.EndAnotherDirection)
            {
                DirectionPlayer.SkipDirection = true;
                DirectionPlayer.DirectionComplete();
            }
            // TODO jaewoo - 연출중일떄 터치 안먹히고 말풍선이 끝나고 먹히도록 수정
            //else
            //{
            //    DirectionPlayer.SkipDirection = true;
            //}
        }

        /// <summary>
        /// 우측상단 스킵 버튼을 눌러서 전체를 스킵할 경우
        /// </summary>
        public void OnClick_AllSkip()
        {
            StoryDirectionComplete();

            if (null != CinemaEndEventFnc)
            {
                CinemaEndEventFnc();
                CinemaEndEventFnc = null;
            }
                
        }

        public void StoryDirectionComplete()
        {
            PlayState = PlayState.complete;
            StopAllCoroutines();
            gameObject.SetActive(false);
        }

        /// <summary>
        /// 스탭들 시작해줌
        /// </summary>
        public IEnumerator StartStoryProp()
        {
            if (false == _isInitialize)
                Initialize();

            if (0 == StepList.Count)
                yield return null;

            yield return StartCoroutine(PlayStep());
        }

        /// <summary>
        /// 다음 스탭을 찾음
        /// </summary>
        public void SetNextStep()
        {
            CurrentStep = StepList.Find(d => d.PlayState == PlayState.wait);
            if (null == CurrentStep)
            {
                return;
            }

            DirectionPlayer.CurrentInfo = CurrentStep;
        }

        IEnumerator PlayStep()
        {
            SetNextStep();

            while (CurrentStep != null)
            {
                foreach (DirectionInfo otherInfo in CurrentStep.OtherDirectionInfos)
                {
                    switch(otherInfo.DirectionType)
                    {
                        //case DirectionType.Disable_TextBallon:
                        //    {
                        //        if (null == otherInfo.DisableStepInfo)
                        //            break;

                        //        StartCoroutine(DirectionPlayer.ShowDirection_ActiveFalse(otherInfo.DisableStepInfo.GetComponent<StepInfo>().TextBallonImageObj.gameObject, false));
                        //        yield return StartCoroutine(DirectionPlayer.ShowDirection_ActiveFalse(otherInfo.DisableStepInfo.GetComponent<StepInfo>().TextObj.gameObject, false));
                        //    }
                        //    break;
                        //case DirectionType.Enable_TextBallon:
                        //    {
                        //        if (null == otherInfo.EnableStepInfo)
                        //            break;

                        //        StartCoroutine(DirectionPlayer.ShowDirection_ActiveTrue(otherInfo.EnableStepInfo.GetComponent<StepInfo>().TextBallonImageObj.gameObject, false));
                        //        yield return StartCoroutine(DirectionPlayer.ShowDirection_ActiveTrue(otherInfo.EnableStepInfo.GetComponent<StepInfo>().TextObj.gameObject, false));
                        //    }
                        //    break;
                        default:
                            yield return StartCoroutine(DirectionPlayer.ShowDirection(otherInfo));
                            break;
                    }
                    
                    while (DirectionPlayer.otherinfoComplete == false)
                    {
                        yield return null;
                    }
                }

                if(false == String.IsNullOrEmpty(CurrentStep.EndString))
                {
                    if (null != CurrentStep.TextBallonImageObj)
                    {
                        TextBallonResize comp = CurrentStep.BallonObj.GetComponent<TextBallonResize>();
                        if (null != comp)
                        {
                            comp.SetTextMaxSize(CurrentStep.EndString);
                        }

                        yield return StartCoroutine(DirectionPlayer.ShowDirection_ActiveTrue(CurrentStep.TextBallonImageObj.gameObject, false));
                    }

                    if (null != CurrentStep.TextObj)
                    {
                        CurrentStep.TextObj.text = "";
                        yield return StartCoroutine(DirectionPlayer.ShowDirection_ActiveTrue(CurrentStep.TextObj.gameObject, false));
                        yield return StartCoroutine(DirectionPlayer.ShowDirection_StringAni());
                    }
                }
                else
                {
                    DirectionPlayer.EndAnotherDirection = true;
                    OnClick_StepComplete();
                }

                while (DirectionPlayer.stepinfoComplete == false)
                {
                    yield return null;
                }

                SetNextStep();
                yield return null;
            }

            StoryDirectionComplete();
            yield return null;
        }

    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StoryMaker
{
    [System.Serializable]
    public class DirectionInfoObject : MonoBehaviour
    {
        public DirectionInfo directionInfo = new DirectionInfo();

        public void SetDirectionInfo(DirectionInfo mdirection)
        {
            directionInfo = mdirection;
        }
        public DirectionInfo GetDirectionInfo()
        {
            return directionInfo;
        }
    }

}

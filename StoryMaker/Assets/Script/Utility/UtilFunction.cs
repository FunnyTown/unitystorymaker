﻿using Soulark.Utility;
using System;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UtilFunction
{
    /// <summary>
    /// 1분 -> 초
    /// </summary>
    const double SECONDS_PER_MINUTE = 60.0f;
    /// <summary>
    /// 1시간 -> 초
    /// </summary>
    const double SECONDS_PER_HOUR = 3600.0f;
    /// <summary>
    /// 1시간 -> 분
    /// </summary>
    const double MINUTES_PER_HOUR = 60.0f;

    public const string QuickChattingPresetKey = "QuickChattingPresetKey_";
    public const string QuickChattingEmoticonKey = "QuickChattingEmoticonKey_";
    public const string TownCameraZoomKey = "TownCameraZoomKey";

    public enum DateTimeStringType
    {
        HHMMSS,
        HHMM,
        MMSS,
    }
    
    /// <summary>
    /// 초(Sec)를 00:00 시:분 / 00:00 분:초 변환
    /// </summary>
    /// <param name="totalSec"></param>
    /// <param name="eType"></param>
    /// <returns></returns>
    public static string GetTimeToColonString(double totalSec, DateTimeStringType eType)
    {
        TimeSpan timeSpan = TimeSpan.FromSeconds(totalSec);

        switch (eType)
        {
            case DateTimeStringType.HHMMSS:
                return string.Format("{0:00}:{1:00}:{2:00}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
            case DateTimeStringType.HHMM:
                return string.Format("{0:00}:{1:00}", timeSpan.Hours, timeSpan.Minutes);
            case DateTimeStringType.MMSS:
                return string.Format("{0:00}:{1:00}", timeSpan.Minutes, timeSpan.Seconds);
            default:
                return "";
        }
    }
    
    /// <summary>
    /// 아이템 등급에 대한 이미지 색상 반환
    /// </summary>
    /// <param name="grade"></param>
    /// <returns></returns>
    public static Color GetItemImageColorByGrade(int grade)
    {
        Color32 color = Color.white;

        switch (grade)
        {
            case 0:
                break;
            case 1:
                color = new Color32(0xb0, 0xfe, 0x41, 0xff);
                break;
            case 2:
                color = new Color32(0x4e, 0xff, 0xdc, 0xff);
                break;
            case 3:
                color = new Color32(0xff, 0x54, 0xc1, 0xff);
                break;
            case 4:
                color = new Color32(0xff, 0x84, 0x4e, 0xff);
                break;
            case 5:
                color = new Color32(0xff, 0xd0, 0x52, 0xff);
                break;
            case 6:
                break;
            default:
                break;
        }

        return color;
    }

    /// <summary>
    /// 아이템 등급에 대한 텍스트 색상 반환
    /// </summary>
    /// <param name="grade"></param>
    /// <returns></returns>
    public static Color GetItemTextColorByGrade(int grade)
    {
        Color32 color = Color.white;

        switch (grade)
        {
            case 0:
                break;
            case 1:
                color = new Color32(0x96, 0xff, 0x00, 0xff);
                break;
            case 2:
                color = new Color32(0x00, 0xff, 0xcc, 0xff);
                break;
            case 3:
                color = new Color32(0xff, 0x00, 0xa2, 0xff);
                break;
            case 4:
                color = new Color32(0xff, 0x4e, 0x00, 0xff);
                break;
            case 5:
                color = new Color32(0xff, 0xba, 0x00, 0xff);
                break;
            case 6:
                break;
            default:
                break;
        }

        return color;
    }

    /// <summary>
    /// 아이템 등급에 대한 텍스트 인코딩 색상 반환
    /// </summary>
    /// <param name="grade"></param>
    /// <returns></returns>
    public static string GetItemTextEncodingColorByGrade(int grade)
    {
        string color = "#ffffffff";

        switch (grade)
        {
            case 0:
                break;
            case 1:
                color = "#96ff00ff";// new Color(0x96, 0xff, 0x00);
                break;
            case 2:
                color = "#00ffccff";//new Color(0x00, 0xff, 0xcc);
                break;
            case 3:
                color = "#ff00a2ff";//new Color(0xff, 0x00, 0xa2);
                break;
            case 4:
                color = "#ff4e00ff";//new Color(0xff, 0x4e, 0x00);
                break;
            case 5:
                color = "#ffba00ff";//new Color(0xff, 0xba, 0x00);
                break;
            case 6:
                break;
            default:
                break;
        }

        return color;
    }
    
    /// <summary>
    /// 날이 다른지만 검사 ( 시간 분 초는 제외)
    /// </summary>
    /// <param name="srcTime"></param>
    /// <param name="dstTime"></param>
    /// <returns></returns>
    public static bool CheckDifferentDay(DateTime nowTime, DateTime checkLastTime)
    {
        if (checkLastTime.Year < nowTime.Year)
            return true;
        else if (checkLastTime.Year > nowTime.Year)
            return false;

        if (checkLastTime.Month < nowTime.Month)
            return true;
        else if (checkLastTime.Month > nowTime.Month)
            return false;

        return checkLastTime.Day < nowTime.Day;
    }

    public static Color GetLineColor(int group)
    {
        Color color = Color.white;
        switch (group)
        {
            case 1:
                color = "#22CF1EFF".HexToColor();//GetColor(58, 227, 70, 255);
                break;
            case 2:
                color = "#FFAC11FF".HexToColor(); ;//GetColor(242, 190, 128, 255);
                break;
            case 3:
                color = "#11B4FFFF".HexToColor(); ;//GetColor(125, 201, 234, 255);
                break;
        }
        return color;
    }
    
    /// <summary>
    ///  UI 경로 아래에 있는 오브젝트를 on/off
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="childName"></param>
    /// <param name="isActive"></param>
    /// <returns></returns>
    public static T SetActiveSoularkUIChildObject<T>(string childName, bool isActive = true) where T : Component
    {
        GameObject soularkUIObject = GameObject.Find("SoularkUI");

        GameObject FindChild = soularkUIObject.GetChildObject(childName);
        if (FindChild != null)
        {
            FindChild.SetActive(true);
            T FindT = FindChild.GetComponent<T>();
            FindChild.SetActive(isActive);
            return FindT;

        }
        return null;
    }

    public static Color GetColor(byte r, byte g, byte b, byte a)
    {
        return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
    }

    public static Color GetBossSliderColor(int sliderIndex)
    {
        switch (sliderIndex)
        {
            case 11: return GetColor(255, 185, 0, 220);

            case 10: return GetColor(255, 185, 0, 220);

            case 9: return GetColor(255, 78, 0, 220);

            case 8: return GetColor(255, 1, 162, 220);

            case 7: return GetColor(0, 222, 255, 220);

            case 6: return GetColor(210, 255, 0, 220);

            case 5: return GetColor(255, 185, 0, 220);

            case 4: return GetColor(255, 78, 0, 220);

            case 3: return GetColor(255, 1, 162, 220);

            case 2: return GetColor(0, 222, 255, 220);

            case 1: return GetColor(210, 255, 0, 220);

            default: return GetColor(255, 255, 255, 220);
        }
    }
    
    public static UnityEngine.Vector3 WorldToScreen(UnityEngine.Vector3 world)
    {
        UnityEngine.Vector3 pos = Camera.main.WorldToScreenPoint(world);
        //pos.x += 30f;
        pos.y = -(Screen.height - pos.y);
        return pos;
    }
    
    public static float GetBatteryLevel()
    {
#if UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            try
            {
                using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                {
                    if (unityPlayer == null)
                        return 0f;
                    using (AndroidJavaObject currActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                    {
                        if (null == currActivity)
                            return 0f;
                        using (AndroidJavaObject intentFilter = new AndroidJavaObject("android.content.IntentFilter", new object[] { "android.intent.action.BATTERY_CHANGED" }))
                        {
                            using (AndroidJavaObject batteryIntent = currActivity.Call<AndroidJavaObject>("registerReceiver", new object[] { null, intentFilter }))
                            {
                                int level = batteryIntent.Call<int>("getIntExtra", new object[] { "level", -1 });
                                int scale = batteryIntent.Call<int>("getIntExtra", new object[] { "scale", -1 });

                                // Error checking that probably isn't needed but I added just in case.
                                if (level == -1 || scale == -1)
                                {
                                    return 0.5f;
                                }
                                return ((float)level / (float)scale);
                            }

                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogException(ex);
            }
        }
#endif
        return 0f;
    }

    
    /// <summary>
    /// width 보다 문자열이 길면 짧은 문자열(width보다 짧고 끝에 ...를 붙임)을 반환한다.
    /// 
    /// </summary>
    /// <param name="text"></param>
    /// <param name="textControl"></param>
    /// <param name="width"></param>
    /// <returns></returns>
    public static string GetShortString(string text, Text textControl, int width)
    {
        if (string.IsNullOrEmpty(text) == true)
            return string.Empty;

        int totalLength = 0;

        Font myFont = textControl.font;  //chatText is my Text component

        char[] arr = text.ToCharArray();
        string returnString = string.Empty;

        CharacterInfo tempCharacterInfo = new CharacterInfo();

        foreach (char c in arr)
        {
            myFont.GetCharacterInfo(c, out tempCharacterInfo, textControl.fontSize);

            totalLength += tempCharacterInfo.advance;
            if (totalLength > width)
            {
                returnString += "...";
                break;
            }
            returnString += c;
        }

        return returnString;
    }
    
    
    /// <summary>
    /// , 가 포함된 숫자 문자열을 리턴함
    /// </summary>
    /// <param name="num"></param>
    /// <returns></returns>
    public static string GetCommaNumberString(int num)
    {
        return GetCommaNumberString((long)num);
    }

    public static string GetCommaNumberString(long num)
    {
        return string.Format("{0:#,##0}", num);
    }
    
    public static bool FindAfterDeleteGameObject(string name)
    {
        GameObject findObject = GameObject.Find(name);
        if (findObject == null)
            return false;

        GameObject.Destroy(findObject);
        findObject = null;

        return true;
    }
    public static void Callback_Restart()
    {
        // 재시작
        // Don't Destroy Object 모두 삭제
        // Splash 씬 로드

        FindAfterDeleteGameObject("SoularkUI");
        FindAfterDeleteGameObject("TimeManager");
        FindAfterDeleteGameObject("SoundManager");
        FindAfterDeleteGameObject("GDBManager");
        FindAfterDeleteGameObject("SpriteHolder");
        FindAfterDeleteGameObject("SceneLoadManager");
        FindAfterDeleteGameObject("NetworkManager");
        FindAfterDeleteGameObject("AssetBundleManager");
        FindAfterDeleteGameObject("PatcherManager");

        SceneManager.LoadScene("Splash");


        //Application.Quit();
        //SceneLoadManager.Instance.LoadSceneWithoutLoading("Login");
    }
    
    /// <summary>
    /// Convert Color = > Hex
    /// </summary>
    public static string ColorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    /// <summary>
    /// Convert Hex => Color
    /// </summary>
    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }
    
    /// <summary>
    /// 버프 컨디션 로그
    /// </summary>
    public static bool ShowBuffConditionLog = false;
    public static void DebugLogBuffContidtion(string log)
    {
        if (ShowBuffConditionLog == false)
            return;

        Debug.Log(log);
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Extention
{
    /// <summary>
    /// Child Find by name
    /// </summary>
    /// <param name="root">parent GameObject</param>
    /// <param name="str">Object name</param>
    /// <returns>resutl GameObject</returns>
    public static GameObject GetChildObject(this GameObject root, string str)
    {
        if (root.name == str)
        {
            return root;
        }

        for (int i = 0; i < root.transform.childCount; ++i)
        {
            GameObject result = GetChildObject(root.transform.GetChild(i).gameObject, str);
            if (result != null)
            {
                return result;
            }
        }

        return null;
    }

    /// <summary>
    /// Child Find by name
    /// </summary>
    /// <typeparam name="T">Component Class</typeparam>
    /// <param name="root">parent GameObject</param>
    /// <param name="str">Object name</param>
    /// <returns></returns>
    public static T GetComponent<T>(this GameObject root, string str) where T : Component
    {
        if (root.name.Equals(str))
            return root.GetComponent<T>();

        List<T> comps = new List<T>(root.GetComponentsInChildren<T>(true));
        return comps.Find(d => d.name.Equals(str) == true);
    }

    public static T GetComponent<T>(this Transform root, string str) where T : Component
    {
        if (root.name.Equals(str))
            return root.GetComponent<T>();

        List<T> comps = new List<T>(root.GetComponentsInChildren<T>(true));
        return comps.Find(d => d.name.Equals(str) == true);
    }    

    /// <summary>
    /// Distance
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static float Distance(this Vector3 a, Vector3 b)
    {
        return Vector3.Distance(a, b);
    }

    public static float DistanceXZ(this Vector3 a, Vector3 b)
    {
        a.y = 0f;
        b.y = 0f;
        return Vector3.Distance(a, b);
    }

    public static void SetText( this Text txt, string str )
    {
        if (null == txt)
        {   
            return;
        }

        if (string.IsNullOrEmpty(str))
        {
            txt.text = "";
            return;
        }
   
        txt.text = str.Replace("\\n", "\n");
    }

    public static void SetText( this Text txt, string format, params object[] args)
    {
        if (null != txt)
        {
            if (string.IsNullOrEmpty(format))
            {
                txt.text = string.Empty;
            }
            else
            {
                string str = string.Format(format, args);
                txt.text = str.Replace("\\n", "\n"); ;
            }
        }
    }
    
    /// <summary>
    /// 천 단위 표시
    /// </summary>
    public static void SetUnitText(this Text txt, int amount)
    {
        txt.text = string.Format("{0:#,##0}", amount);
    }

    public static void SetUnitText(this Text txt, long amount)
    {
        txt.text = string.Format("{0:#,##0}", amount);
    }

    public static void SetPersentTex(this Text txt , float amount)
    {
        txt.text = string.Format("{0:F2}%", amount);
    }
    
    /// <summary>
    /// 입력받은 인코딩 색상 값으로 변경
    /// </summary>
    /// <param name="txt"></param>
    /// <param name="encodingColorText"></param>
    public static void SetTextColor(this Text txt, string encodingColorText)
    {
        txt.text = string.Format("<color={0}>{1}</color>", encodingColorText, txt.text);
    }

    /// <summary>
    /// Dictionay find key by value
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="V"></typeparam>
    /// <param name="instance"></param>
    /// <param name="value"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    public static bool TryGetKey<K, V>(this IDictionary<K, V> instance, V value, out K key)
    {
        foreach (var entry in instance)
        {
            if (!entry.Value.Equals(value))
            {
                continue;
            }
            key = entry.Key;
            return true;
        }
        key = default(K);
        return false;
    }

    public static T CopyComponent<T>(this GameObject destination, T original) where T : Component
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy as T;
    }
    
    public static bool IsDestroyed(this GameObject gameObject)
    {
        // UnityEngine overloads the == opeator for the GameObject type
        // and returns null when the object has been destroyed, but 
        // actually the object is still there but has not been cleaned up yet
        // if we test both we can determine if the object has been destroyed.
        return gameObject == null && !ReferenceEquals(gameObject, null);
    }

    public static bool IsNull<T, TU>(this KeyValuePair<T, TU> pair)
    {
        return pair.Equals(new KeyValuePair<T, TU>());
    }

    public static Color32 HexToColor( this string hex )
    {
        hex = hex.Replace("#", "");
        byte a = 255;
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);

        if( hex.Length == 8 )
             a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);

        return new Color32(r, g, b, a);
    }
}

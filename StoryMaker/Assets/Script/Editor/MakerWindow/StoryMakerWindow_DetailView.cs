﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace StoryMaker
{
    /// <summary>
    /// 우측 디테일 화면
    /// </summary>
    partial class StoryMakerWindow : EditorWindow
    {
        List<BaseNode> nodelist = new List<BaseNode>();

        Vector2 _mousePos;
        Vector2 NodeScrollPos;
        StepInfo SelectedStep;

        /// 노드 윈도우 그리는 부분
        #region Node Window Draw
        void OnDraw_DetailWindow(int id)
        {
            NodeScrollPos = EditorGUILayout.BeginScrollView(NodeScrollPos, GUILayout.Width(window_DetailRect.width - 10), GUILayout.Height(window_DetailRect.height-30));
            
            if (false == IsHaveSelectStep())
            {
                //if(null != Model.GetSelectProp())
                //    ChangeSelectStepInfo(Model.GetFirstStepInfo());

                StoryMakerManager.DrawNodeLineReset();
                GUILayout.BeginArea(new Rect(10, 10, window_DetailRect.width, window_DetailRect.height));
                OnDraw_DetailNocie("Select Stepinfo Plz");
                GUILayout.EndArea();
            }
            else
            {
                EditorGUILayout.BeginHorizontal();

                OnDraw_DetailName();
                GUILayout.Space(5f);
                //OnDraw_DetailOptionButton();

                EditorGUILayout.EndHorizontal();

                OnCheck_MouseClick();
                OnDraw_Node();
            }

            OnDraw_NodeLineList();
            OnDraw_NewNodeLine();
            
            EditorGUILayout.BeginHorizontal();
            if (nodelist.Count > 0)
            {
                for(int nodeCount = 3; nodelist.Count >= nodeCount; nodeCount++)
                {
                    GUILayout.Label(StoryMakerManager.String_DetailScroll, "LODLevelNotifyText");
                    //EditorGUILayout.TextArea(StoryMakerManager.String_DetailScroll, "LODLevelNotifyText");
                }
            }
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.BeginVertical();
            for (int nodeCount = 0; nodeCount < 30; nodeCount++)
            {
                GUILayout.Label("", "LODLevelNotifyText");
                //EditorGUILayout.LabelField(" ", " ", "LODLevelNotifyText");
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndScrollView();

            Repaint();
        }

        public bool IsHaveSelectStep()
        {
            if (null == Model.GetSelectProp())
                return false;

            if (null == SelectedStep)
                return false;

            return true;
        }

        public void Update()
        {
            OnCheck_MouseClick();
        }

        /// <summary>
        /// 스탭 이름 텍스트 필드
        /// </summary>
        void OnDraw_DetailName()
        {
            GUILayout.Label("Step Name :", GUILayout.Width(75));
            SelectedStep.StepName = EditorGUILayout.TextField(SelectedStep.StepName, GUILayout.Width(150));
        }

        /// <summary>
        /// 옵션 버튼 Draw
        /// </summary>
        //void OnDraw_DetailOptionButton()
        //{
        //    if(GUILayout.Button("Option", "GV Gizmo DropDown"))
        //    {
        //        DropDownOption();
        //    }
        //}

        /// <summary>
        /// 노드 Draw
        /// </summary>
        void OnDraw_Node()
        {
            for (int i = 0; i < nodelist.Count; i++)
            {
                nodelist[i].OnDrawNode();
            }

            Repaint();
        }

        #endregion

        /// 마우스 오른쪽 클릭 이벤트
        #region Mouse Right Click

        /// <summary>
        /// 마우스 우클릭 
        /// </summary>
        void OnCheck_MouseClick()
        {
            if (null == SelectedStep)
                return;

            Event e = Event.current;
            if (null == e)
                return;

            OnMouseClick_KeyboardFocusOut();

            List<BaseNode> reverseList = new List<BaseNode>(nodelist);
            reverseList.Reverse();

            _mousePos = e.mousePosition;
            
            if (e.button == 1 && e.type == EventType.MouseDown)
            {
                foreach (BaseNode item in reverseList)
                {
                    if (item.Type == NodeType.Start || item.Type == NodeType.End)
                        continue;

                    if (item.DrawRect.Contains(_mousePos))
                    {
                        //SelectNode = item;
                        ShowDeleteNodePopup(item);
                        //OnNodeDelete = true;
                        return;
                    }
                }

                //OpenNodeMenu = true;
                OnDraw_CreateDirectionMenu();
                e.Use();
            }
        }

        private void ShowDeleteNodePopup(BaseNode selectNode)
        {
            if (EditorUtility.DisplayDialog("Node Delete", "선택하신 노드를 삭제하시겠습니까?", "Yes", "No"))
            {
                if (selectNode.IsMulti())
                    RemoveMultiNode(selectNode);
                else
                    RemoveNode(selectNode);
            }
            else { }
        }

        private void RemoveNode(BaseNode node)
        {
            bool isRemove = false;
            foreach (BaseNode item in nodelist)
            {
                if (item.NextNode == node)
                    item.NextNode = null;
                if(isRemove)
                {
                    item.DrawRect.x -= 230;
                }
            }

            DeleteDirectionObject(node.DirectionInfo);
            SelectedStep.OtherDirectionInfos.Remove(node.DirectionInfo);
            nodelist.Remove(node);
        }

        private void RemoveMultiNode(BaseNode node)
        {
            BaseNode findMultiNode;
            foreach (BaseNode item in nodelist)
            {
                findMultiNode = item.NextMultiNode.FirstOrDefault(d => d == node);
                if (null != findMultiNode)
                {
                    item.NextMultiNode.Remove(node);
                }
            }

            DeleteDirectionObject(node.DirectionInfo);
            SelectedStep.OtherDirectionInfos.Remove(node.DirectionInfo);
            nodelist.Remove(node);
        }

        private void DeleteDirectionObject(DirectionInfo deleteInfo)
        {
            Transform StepObj = GetSelectStepObj();
            if (null == StepObj)
                return;

            DirectionInfoObject[] StepObjts = StepObj.GetComponentsInChildren<DirectionInfoObject>();
            GameObject findObj = StepObjts.FirstOrDefault(d => d.directionInfo == deleteInfo).gameObject;
            if (null != findObj)
                DestroyImmediate(findObj);
        }

        /// <summary>
        /// 노드 백그라운드에서 우클릭시 보여주는 메뉴 팝업
        /// </summary>
        void OnDraw_CreateDirectionMenu()
        {
            GenericMenu menu = new GenericMenu();

            menu.AddItem(new GUIContent("TextBallon 추가"), false, ResourceAdd, RecourceType.TextBallon);
            menu.AddItem(new GUIContent("Image 추가"), false, ResourceAdd, RecourceType.Image);
            menu.AddItem(new GUIContent("Text 추가"), false, ResourceAdd, RecourceType.Text);

            menu.AddSeparator("");
            
            menu.AddItem(new GUIContent("Move"), false, ContextCallback, DirectionType.Move);
            menu.AddItem(new GUIContent("Change Color"), false, ContextCallback, DirectionType.ChangeColor);
            menu.AddItem(new GUIContent("Jump"), false, ContextCallback, DirectionType.Jump);
            menu.AddItem(new GUIContent("Shake"), false, ContextCallback, DirectionType.Shake);
            menu.AddItem(new GUIContent("Bounce"), false, ContextCallback, DirectionType.Bounce);
            menu.AddItem(new GUIContent("Swap"), false, ContextCallback, DirectionType.Swap);
            menu.AddItem(new GUIContent("Rotate"), false, ContextCallback, DirectionType.Rotation);
            menu.AddItem(new GUIContent("Scale"), false, ContextCallback, DirectionType.Scale);
            menu.AddItem(new GUIContent("FadeInOut"), false, ContextCallback, DirectionType.FadeInOut);

            menu.AddSeparator("");

            menu.AddItem(new GUIContent("Spine Ani"), false, ContextCallback, DirectionType.SpineAni);
            menu.AddItem(new GUIContent("Active"), false, ContextCallback, DirectionType.Active);
            menu.AddItem(new GUIContent("Enable"), false, ContextCallback, DirectionType.Enable);

            menu.ShowAsContext();
        }

        /// <summary>
        /// 이미지, 텍스트 리소스 추가 부분
        /// </summary>
        void ResourceAdd(object obj)
        {
            RecourceType id = (RecourceType)obj;

            if (id.Equals(RecourceType.Image))
            {
                GameObject resourcePrefab = AssetDatabase.LoadAssetAtPath(StoryMakerManager.ImagePrefabPath, typeof(GameObject)) as GameObject;
                if (null == resourcePrefab)
                    return;

                GameObject newResObj = Instantiate(resourcePrefab, Model.GetImageParent());
                newResObj.name = "new Image";
                newResObj.transform.localPosition = Vector3.zero;
            }
            else if (id.Equals(RecourceType.Text))
            {
                GameObject resourcePrefab = AssetDatabase.LoadAssetAtPath(StoryMakerManager.TextPrefabPath, typeof(GameObject)) as GameObject;
                if (null == resourcePrefab)
                    return;

                GameObject newResObj = Instantiate(resourcePrefab, Model.GetTextParent());
                newResObj.name = "new Text";
                newResObj.transform.localPosition = Vector3.zero;
            }
            else if (id.Equals(RecourceType.TextBallon))
            {
                GameObject resourcePrefab = AssetDatabase.LoadAssetAtPath(StoryMakerManager.TextBallonPrefabPath, typeof(GameObject)) as GameObject;
                if (null == resourcePrefab)
                    return;

                GameObject newResObj = Instantiate(resourcePrefab, Model.GetTextBallonParent());
                newResObj.name = "new TextBallon";
                newResObj.transform.localPosition = Vector3.zero;
            }
        }

        /// <summary>
        /// 메뉴팝업 콜백 
        /// </summary>
        void ContextCallback(object obj)
        {
            DirectionType id = (DirectionType)obj;

            BaseNode newNode = new BaseNode();

            if (id.Equals(DirectionType.Move))
            {
                newNode = new MoveNode();
                ((MoveNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.Move);
            }
            else if (id.Equals(DirectionType.ChangeColor))
            {
                newNode = new ColorNode();
                ((ColorNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.ChangeColor);
            }
            else if (id.Equals(DirectionType.Jump))
            {
                newNode = new JumpNode();
                ((JumpNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.Jump);
            }
            else if (id.Equals(DirectionType.Shake))
            {
                newNode = new ShakeNode();
                ((ShakeNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.Shake);
            }
            else if (id.Equals(DirectionType.Bounce))
            {
                newNode = new BounceNode();
                ((BounceNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.Bounce);
            }
            else if (id.Equals(DirectionType.Swap))
            {
                newNode = new SwapNode();
                ((SwapNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.Swap);
            }
            else if (id.Equals(DirectionType.Active))
            {
                newNode = new ActiveNode();
                ((ActiveNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.Active);
            }
            else if (id.Equals(DirectionType.Rotation))
            {
                newNode = new RotationNode();
                ((RotationNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.Rotation);
            }
            else if (id.Equals(DirectionType.Scale))
            {
                newNode = new ScaleNode();
                ((ScaleNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.Scale);
            }
            else if (id.Equals(DirectionType.FadeInOut))
            {
                newNode = new FadeInOutNode();
                ((FadeInOutNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.FadeInOut);
            }
            else if (id.Equals(DirectionType.Enable))
            {
                newNode = new EnableNode();
                ((EnableNode)newNode).Init();
                newNode.DirectionInfo = AddDirection(DirectionType.Enable);
            }

            newNode.DrawRect.x = _mousePos.x;
            newNode.DrawRect.y = _mousePos.y;

            newNode.NodeSelectAction_Left = OnClick_NodeStart;
            newNode.NodeSelectAction_Right = OnClick_NodeNext;
            
            if (null == newNode.DirectionInfo)
                return;

            nodelist.Add(newNode);
        }

        #endregion
        
        /// 우측 디테일뷰 옵션 버튼 및 "저장" 관련
        #region DetailView Option
        //private void DropDownOption()
        //{
        //    GenericMenu menu = new GenericMenu();
        //    menu.AddItem(new GUIContent("Save Step"), false, DetailOptionCallBack, DetailOptionType.Save);
        //    menu.ShowAsContext();
        //}

        ///// <summary>
        ///// 옵션 콜백
        ///// </summary>
        //private void DetailOptionCallBack(object obj)
        //{
        //    DetailOptionType id = (DetailOptionType)obj;
        //    switch(id)
        //    {
        //        case DetailOptionType.Save:
        //            {
        //                SaveStepInfo();
        //            }
        //            break;
        //        default:
        //            break;
        //    }
        //}

        /// <summary>
        /// 저장하기
        /// </summary>
        private void SaveStepInfo()
        {
            BaseNode findNode = nodelist.Where(d => d.IsMulti() == false && d.Type != NodeType.End)
                                        .FirstOrDefault(d => d.NextNode == null);

            if(null != findNode)
            {
                EditorUtility.DisplayDialog("Save Fail", "끊어진 노드가 있어서 Save 에 실패했습니다.", "OK", "");
                return;
            }

            List<DirectionInfo> newDirectionList = new List<DirectionInfo>();

            if(null == SelectedStep)
            {
                EditorUtility.DisplayDialog("Save Fail", "선택된 Step 정보가 없어서 save 에 실패했습니다.", "OK", "");
                return;
            }

            SelectedStep.OtherDirectionInfos.Clear();
            
            foreach (BaseNode info in nodelist)
            {
                if (info.NextNode == null)
                    continue;

                if (info.NextNode.Type == NodeType.End)
                {
                    foreach (BaseNode multiInfo in info.NextMultiNode)
                    {
                        newDirectionList.Add(multiInfo.DirectionInfo);
                    }
                    continue;
                }
                
                foreach (BaseNode multiInfo in info.NextMultiNode)
                {
                    newDirectionList.Add(multiInfo.DirectionInfo);
                }
                newDirectionList.Add(info.NextNode.DirectionInfo);
            }
            
            SaveDirectionList(newDirectionList);
            SavePrefabApply();
            EditorUtility.SetDirty(Model.GetSelectProp());
        }

        /// <summary>
        /// 연출 리스트 저장 ( 스탭의 연출 리스트에 하나하나 저장해주는 부분 )
        /// </summary>
        void SaveDirectionList(List<DirectionInfo> newDirectionList)
        {
            Transform StepObj = GetSelectStepObj();
            if (null == StepObj)
                return;

            DirectionInfoObject[] StepObjts = StepObj.GetComponentsInChildren<DirectionInfoObject>();
            for(int index = 0; index < StepObjts.Length; index++)
            {
                StepObjts[index].name = string.Format("{0}_{1}", newDirectionList[index].DirectionType, index);
                StepObjts[index].directionInfo = newDirectionList[index];

                SelectedStep.OtherDirectionInfos.Add(newDirectionList[index]);
            }
        }

        /// <summary>
        /// 프리팹 으로 저장해줌 ( 없으면 만들어서 저장 )
        /// </summary>
        public void SavePrefabApply()
        {
            string localPath = "Assets/BuildAssetBundlesSourcePath/StoryMaker/Props/" + Model.GetSelectProp().name + ".prefab";
            if (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject)))
            {
                PrefabUtility.ReplacePrefab(Model.GetSelectProp().gameObject, PrefabUtility.GetPrefabParent(Model.GetSelectProp()), ReplacePrefabOptions.ConnectToPrefab);
                EditorUtility.DisplayDialog("Save Sucess", "프랍 정보가 저장되었습니다.", "OK", "");

            }
            else
            {
                Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
                PrefabUtility.ReplacePrefab(Model.GetSelectProp().gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);

                EditorUtility.DisplayDialog("Save Sucess", "새로운 프랍을 생성했습니다.", "OK", "");
            }
            AssetDatabase.Refresh();
        }
        
        /// <summary>
        /// 노드 화면에 오류 공지 출력
        /// </summary>
        void OnDraw_DetailNocie(string notice)
        {
            GUIStyle fontStyle = new GUIStyle();

            fontStyle.fontSize = 25;
            fontStyle.normal.textColor = Color.red;
            GUI.Label(new Rect(10, 0, 300, 100), notice, fontStyle);
        }
        
        /// <summary>
        /// 연출 추가
        /// </summary>
        DirectionInfo AddDirection(DirectionType directionType)
        {
            if (null == SelectedStep)
                return null;

            DirectionInfo newDirection = new DirectionInfo();
            newDirection.DirectionType = directionType;
            
            GameObject resourcePrefab = AssetDatabase.LoadAssetAtPath(StoryMakerManager.DirectionPrefabPath, typeof(GameObject)) as GameObject;
            if (null == resourcePrefab)
                return null;

            GameObject newResObj = Instantiate(resourcePrefab, GetSelectStepObj());
            newResObj.name = "new Direction";
            newResObj.transform.localPosition = Vector3.zero;
            newResObj.GetComponent<DirectionInfoObject>().SetDirectionInfo(newDirection);

            return newDirection;
        }

        /// <summary>
        /// 현재 선택된 스탭정보를 가지고 있는 GameObject 를 하이러키 에서 찾아옴
        /// </summary>
        Transform GetSelectStepObj()
        {
            StepInfoObject[] StepObjts = Model.GetSelectProp().GetComponentsInChildren<StepInfoObject>();
            StepInfoObject findStepObj = StepObjts.FirstOrDefault(d => d.GetComponent<StepInfoObject>().GetStepInfo() == SelectedStep);
            if (null != findStepObj)
                return findStepObj.transform;
            else
                return null;
        }

        #endregion

        /// 노드 좌, 우측 연결 버튼 콜백
        #region Node Connect Button Callback

        /// <summary>
        /// 노드 좌측 연결 버튼 클릭
        /// </summary>
        public void OnClick_NodeStart(BaseNode selectNode)
        {
            /// start -> null 일 경우
            if (IsFirstDraw(selectNode, NodeDrawPos.Start))
                return;

            ///  같은 노드의 같은 자리를 두번 눌렀을경우
            if (IsSameNode(selectNode, NodeDrawPos.Start))
                return;

            /// start -> 왼쪽 , next -> 왼쪽 일 경우
            if (IsSamePos(NodeDrawPos.Start))
                return;

            /// 이미 서로 연결되있는 노드일 경우
            if (IsConnectNode(selectNode))
                return;
            
            /// 지금 내가 연결하고싶은 다음 노드가 멀티 노드일 경우
            if (true == selectNode.IsMulti())
            {
                /// 이미 다른데서 사용중일경우
                if (CheckUseMultiNode(selectNode, NodeDrawPos.Start))
                {
                    StoryMakerManager.DrawNodeLineEnd();
                    return;
                }
                else
                {
                    StoryMakerManager.DrawingInfo.NextInfo.NextMultiNode.Add(selectNode);
                }
            }
            else
            {
                CheckChangeLine_Start(selectNode);

                StoryMakerManager.DrawingInfo.NextInfo.NextNode = selectNode;
            }
            
            StoryMakerManager.DrawNodeLineEnd();
        }

        /// <summary>
        /// 노드 우측 연결 버튼 클릭
        /// </summary>
        public void OnClick_NodeNext(BaseNode selectNode)
        {
            /// start -> null 일 경우
            if (IsFirstDraw(selectNode, NodeDrawPos.Next))
                return;

            ///  같은 노드의 같은 자리를 두번 눌렀을경우
            if (IsSameNode(selectNode, NodeDrawPos.Next))
                return;

            /// start -> 오른쪽 , next -> 오른쪽 일 경우
            if (IsSamePos(NodeDrawPos.Next))
                return;
            
            /// 이미 서로 연결되있는 노드일 경우
            if (IsConnectNode(selectNode))
                return;
            
            /// 시작 노드가 동시진행 노드일 경우
            if (true == StoryMakerManager.DrawingInfo.StartInfo.IsMulti())
            {
                /// 이미 다른데서 사용중일경우
                if (CheckUseMultiNode(selectNode, NodeDrawPos.Next))
                    return;
                else
                {
                    selectNode.NextMultiNode.Add(StoryMakerManager.DrawingInfo.StartInfo);
                }
            }
            else
            {
                CheckChangeLine_Next(selectNode);

                selectNode.NextNode = StoryMakerManager.DrawingInfo.StartInfo;
            }

            StoryMakerManager.DrawNodeLineEnd();
        }

        #endregion

        /// 노드를 그리는 부분에서 사용하는 예외처리
        #region Draw Node Line Controll

        /// <summary>
        /// 라인을 처음 그리는 단계인지
        /// </summary>
        private bool IsFirstDraw(BaseNode selectNode, NodeDrawPos pos)
        {
            if (StoryMakerManager.DrawingInfo.isDrawing == false)
            {
                if(pos == NodeDrawPos.Start)
                {
                    StoryMakerManager.DrawingInfo.isSelectStart= true;
                    StoryMakerManager.SetStartNode(selectNode, NodeDrawPos.Start);
                }
                else
                {
                    StoryMakerManager.DrawingInfo.isSelectNext = true;
                    StoryMakerManager.SetStartNode(selectNode, NodeDrawPos.Next);
                }
                
                return true;
            }
            return false;
        }

        /// <summary>
        /// 같은 방향의 시작점을 선택했는지
        /// Start 를 누르고 다른 노드의 Start 를 눌렀을 경우 
        /// or Next 를 누르고 다른 노드의 Next 를 누른 경우
        /// </summary>
        private bool IsSamePos(NodeDrawPos pos)
        {
            if (StoryMakerManager.IsSamePosition(pos))
            {
                StoryMakerManager.DrawNodeLineEnd();
                return true;
            }
            return false;
        }

        /// <summary>
        /// 나로 시작해서 나로 끝날경우
        /// </summary>
        private bool IsSameNode(BaseNode selectNode, NodeDrawPos pos)
        {
            if (StoryMakerManager.DrawingInfo.StartInfo != selectNode)
            {
                if(StoryMakerManager.DrawingInfo.NextInfo == selectNode && pos == NodeDrawPos.Start)
                {
                    StoryMakerManager.DrawNodeLineEnd();
                    return true;
                }
                return false;
            }

            switch (pos)
            {
                case NodeDrawPos.Start:
                    {
                        if (false == selectNode.IsMulti())
                        {
                            List<BaseNode> findNodeList = nodelist.Where(d => d.NextNode == selectNode).ToList();
                            foreach (BaseNode nodeInfo in findNodeList)
                            {
                                nodeInfo.NextNode = null;
                            }
                        }
                        else
                        {
                            BaseNode findMultinode;
                            foreach (BaseNode nodeinfo in nodelist)
                            {
                                if (false == nodeinfo.IsMulti())
                                {
                                    findMultinode = nodeinfo.NextMultiNode.FirstOrDefault(d => d == selectNode);
                                    if (null != findMultinode)
                                    {
                                        nodeinfo.NextMultiNode.Remove(selectNode);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case NodeDrawPos.Next:
                    {
                        if(StoryMakerManager.DrawingInfo.isSelectStart == true)
                        {
                            StoryMakerManager.DrawNodeLineEnd();
                            return true;
                        }
                    }
                    break;
            }

            StoryMakerManager.DrawNodeLineEnd();
            return true;
        }

        /// <summary>
        /// 이미 서로 연결된 노드인지
        /// </summary>
        private bool IsConnectNode(BaseNode selectNode)
        {
            if (StoryMakerManager.DrawingInfo.StartInfo != null)
            {
                if (StoryMakerManager.DrawingInfo.StartInfo.NextNode == selectNode)
                {
                    StoryMakerManager.DrawNodeLineEnd();
                    return true;
                }
            }
            else if (StoryMakerManager.DrawingInfo.NextInfo != null)
            {
                if (StoryMakerManager.DrawingInfo.NextInfo.NextNode == selectNode)
                {
                    StoryMakerManager.DrawNodeLineEnd();
                    return true;
                }
            }
            
            return false;
        }

        /// <summary>
        /// 이미 연결되있는 노드인지 체크해서 
        /// 기존에 연결된 부분을 끊어주고 새로 연결시켜줌
        /// </summary>
        private void CheckChangeLine_Next(BaseNode selectNode)
        {
            BaseNode findNode = nodelist.FirstOrDefault(d => d.NextNode == StoryMakerManager.DrawingInfo.StartInfo);
            if (null != findNode)
                findNode.NextNode = null;
        }

        private void CheckChangeLine_Start(BaseNode selectNode)
        {
            BaseNode findNode = nodelist.FirstOrDefault(d => d.NextNode == selectNode);
            if (null != findNode)
                findNode.NextNode = null;
        }

        /// <summary>
        /// 멀티 노드가 이미 다른곳에서 사용중일경우
        /// </summary>
        public bool CheckUseMultiNode(BaseNode selectNode, NodeDrawPos pos)
        {
            BaseNode findNode;

            if(pos == NodeDrawPos.Start)
            {
                foreach (BaseNode nodeInfo in nodelist)
                {
                    findNode = nodeInfo.NextMultiNode.FirstOrDefault(d => d == selectNode);
                    if (null != findNode)
                        return true;
                }
                return false;
            }
            else
            {
                foreach (BaseNode nodeInfo in nodelist)
                {
                    findNode = nodeInfo.NextMultiNode.FirstOrDefault(d => d == StoryMakerManager.DrawingInfo.StartInfo);
                    if (null != findNode)
                        return true;
                }
                return false;
            }
            
        }

        #endregion

        /// 노드 그리기
        #region Draw Node Line
        /// <summary>
        /// 지금 연결하고자 하는 라인을 그림
        /// </summary>
        void OnDraw_NewNodeLine()
        {
            if (false == StoryMakerManager.isDrawing())
                return;
            
            Vector2 mousePos = Event.current.mousePosition;
            
            Rect start = StoryMakerManager.GetNodeCurveStartRect();

            Vector3 startPos = new Vector3(start.x, start.y, 0);
            Vector3 endPos = new Vector3(mousePos.x, mousePos.y, 0);
            Vector3 startTan = startPos + Vector3.right * 50;
            Vector3 endTan = endPos + Vector3.left * 50;
            
            Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.white, null, 5);            
        }

        /// <summary>
        /// 연결되어있는 라인을 그림
        /// </summary>
        void OnDraw_NodeLineList()
        {
            foreach(BaseNode nodeItem in nodelist)
            {
                if (null != nodeItem.NextNode)
                {
                    Rect start = nodeItem.GetDrawLineStartPos();
                    Rect end = nodeItem.NextNode.GetDrawLineEndPos();

                    Vector3 startPos = new Vector3(start.x, start.y, 0);
                    Vector3 endPos = new Vector3(end.x, end.y, 0);
                    Vector3 startTan = startPos + Vector3.right * 50;
                    Vector3 endTan = endPos + Vector3.left * 50;

                    Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.white, null, 5);
                }
                
                foreach( BaseNode multinode in nodeItem.NextMultiNode)
                {
                    Rect start = nodeItem.GetDrawLineStartPos();
                    Rect end = multinode.GetDrawLineEndPos();

                    Vector3 startPos = new Vector3(start.x, start.y, 0);
                    Vector3 endPos = new Vector3(end.x, end.y, 0);
                    Vector3 startTan = startPos + Vector3.right * 50;
                    Vector3 endTan = endPos + Vector3.left * 50;
                    
                    Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.white, null, 5);
                }
            }
        }
    }
#endregion
}

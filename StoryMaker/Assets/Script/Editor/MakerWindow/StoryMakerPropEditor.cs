﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;

namespace StoryMaker
{
    //[CustomEditor(typeof(StoryProp))]
    //public class StoryMakerPropEditor : Editor
    //{
    //    StoryProp _propScript;

    //    void OnEnable()
    //    {
    //        _propScript = target as StoryProp;
    //    }

    //    public override void OnInspectorGUI()
    //    {
    //        GUILayout.Space(5);
    //        GUILayout.BeginHorizontal();

    //        OnDraw_ResourceAddButton();

    //        GUILayout.EndHorizontal();
    //        GUILayout.Space(2);

    //        foreach (StepInfo info in new List<StepInfo>(_propScript.StepList))
    //        {
    //            OnDraw_Seperator(Color.white);
    //            OnDraw_Seperator(Color.white);

    //            GUILayout.BeginHorizontal();

    //            OnDraw_StepButtons(info);

    //            GUILayout.EndHorizontal();

    //            // Setting ImageObject or TextObject
    //            OnDraw_ResourceSetting(info);

    //            // 진행 시간
    //            OnDraw_Duration(info);
                
    //            GUILayout.BeginHorizontal();

    //            GUI.color = Color.yellow;
    //            GUILayout.TextArea("Direction List", "ObjectPickerLargeStatus");
    //            GUI.color = Color.white;

    //            if (GUILayout.Button("ADD Direction"))
    //            {
    //                DirectionInfo newDirectionInfo = new DirectionInfo();
    //                info.OtherDirectionInfos.Add(newDirectionInfo);
    //            }

    //            GUILayout.EndHorizontal();
                
    //            foreach (DirectionInfo DirectionInfoItem in new List<DirectionInfo>(info.OtherDirectionInfos))
    //            {
    //                OnDraw_Seperator(Color.yellow);

    //                GUI.color = Color.red;
    //                if (GUILayout.Button("Delete"))
    //                {
    //                    info.OtherDirectionInfos.Remove(DirectionInfoItem);
    //                    continue;
    //                }
    //                GUI.color = Color.white;

    //                DirectionInfoItem.DirectionType = (DirectionType)EditorGUILayout.EnumPopup("Direction Type", DirectionInfoItem.DirectionType);

    //                if (DirectionInfoItem.DirectionType != DirectionType.Disable_TextBallon && DirectionInfoItem.DirectionType != DirectionType.Enable_TextBallon)
    //                {
    //                    DirectionInfoItem.ImageObj = (Image)EditorGUILayout.ObjectField("Other Image", DirectionInfoItem.ImageObj, typeof(Image), true);
    //                }
    //                else if(DirectionInfoItem.DirectionType == DirectionType.Disable_TextBallon)
    //                {
    //                    DirectionInfoItem.DisableStepInfo = (GameObject)EditorGUILayout.ObjectField("Disable TextBallon", DirectionInfoItem.DisableStepInfo, typeof(GameObject), true);
    //                }
    //                else if(DirectionInfoItem.DirectionType == DirectionType.Enable_TextBallon)
    //                {
    //                    DirectionInfoItem.EnableStepInfo = (GameObject)EditorGUILayout.ObjectField("Enable TextBallon", DirectionInfoItem.EnableStepInfo, typeof(GameObject), true);
    //                }

    //                GUILayout.BeginHorizontal();
    //                DirectionInfoItem.Duration = EditorGUILayout.FloatField("Duration", DirectionInfoItem.Duration);
    //                GUILayout.EndHorizontal();

    //                DirectionInfoItem.IsMultiPlay = EditorGUILayout.Toggle("Is Multy for Next Ani", DirectionInfoItem.IsMultiPlay);

    //                if (DirectionInfoItem.DirectionType == DirectionType.Move)
    //                {
    //                    DirectionInfoItem.EndPos = EditorGUILayout.Vector2Field("End Position", DirectionInfoItem.EndPos);
    //                }
    //                if (DirectionInfoItem.DirectionType == DirectionType.ChangeColor)
    //                {
    //                    DirectionInfoItem.Color = EditorGUILayout.ColorField("End Color", DirectionInfoItem.Color);
    //                }
    //                if (DirectionInfoItem.DirectionType == DirectionType.Jump)
    //                {
    //                    DirectionInfoItem.JumpHeight = EditorGUILayout.FloatField("Updown Height", DirectionInfoItem.JumpHeight);
    //                }
    //                if (DirectionInfoItem.DirectionType == DirectionType.Bounce)
    //                {
    //                    DirectionInfoItem.EndPos = EditorGUILayout.Vector2Field("End Position", DirectionInfoItem.EndPos);
    //                    DirectionInfoItem.BounceCount = EditorGUILayout.IntField("Jump Count", DirectionInfoItem.BounceCount);
    //                }
    //            }
                
    //            GUI.color = Color.white;
    //        }
            
    //        OnDraw_Seperator(Color.white);
            
    //        Repaint();
    //        EditorUtility.SetDirty(_propScript.gameObject);
    //    }

    //    void OnDraw_ResourceAddButton()
    //    {
    //        if (GUILayout.Button("ADD Step"))
    //        {
    //            GameObject resourcePrefab = Resources.Load(StoryMakerManager.StepPrefabPath) as GameObject;
    //            if (null == resourcePrefab)
    //                return;

    //            GameObject newResObj = Instantiate(resourcePrefab, _propScript.transform);
    //            newResObj.name = "new Step";
    //            newResObj.transform.localPosition = Vector3.zero;

    //            StepInfo newStep = newResObj.GetComponent<StepInfo>();

    //            GameObject findObj;
    //            findObj = newResObj.transform.GetChild(0).gameObject;

    //            if(null != findObj)
    //            {
    //                Image findImage;
    //                findImage = findObj.GetComponent<Image>();
    //                if (null != findImage)
    //                    newStep.TextBallonImageObj = findImage;
    //            }

    //            findObj = newResObj.transform.GetChild(1).gameObject;
    //            if(null != findObj)
    //            {
    //                Text findText;
    //                findText = findObj.GetComponent<Text>();
    //                if (null != findText)
    //                    newStep.TextObj = findText.GetComponent<Text>();
    //            }

    //            findObj = newResObj.transform.GetChild(2).gameObject;
    //            if (null != findObj)
    //            {
    //                Image findImage;
    //                findImage = findObj.GetComponent<Image>();
    //                if (null != findImage)
    //                    newStep.TouchImageObj = findImage.GetComponent<Image>();
    //            }
                
    //            _propScript.StepList.Add(newStep);
    //        }
    //    }
        
        
    //    /// <summary>
    //    /// 구분선
    //    /// </summary>
    //    void OnDraw_Seperator(Color color)
    //    {
    //        EditorGUILayout.Space();

    //        //Texture2D tex = new Texture2D(1, 1);  //1 by 1 Pixel
    //        //GUI.color = color;
    //        //float y = GUILayoutUtility.GetLastRect().yMax;  //마지막 구성요소의 사각형
    //        //GUI.DrawTexture(new Rect(0.0f, y, Screen.width, 1.0f), tex);  //인스펙터창의 가로사이즈 만큼 라인을 그린다.
    //        //tex.hideFlags = HideFlags.DontSave;  //매번 정보가 변경될 때 마다 인스펙터 창이 새로 랜더링 되므로 저장을 방지하여 메모리 누수를 방지한다.
    //        //GUI.color = Color.white;  //기본 색상 화이트로 초기화 해준다. 
    //    }

    //    /// <summary>
    //    /// 텍스트 리소스 추가
    //    /// </summary>
    //    void AddResourceInfo_Text(StepInfo info)
    //    {
    //        GameObject resourcePrefab = Resources.Load(StoryMakerManager.TextPrefabPath) as GameObject;
    //        if (null == resourcePrefab)
    //            return;

    //        GameObject newResObj = Instantiate(resourcePrefab, _propScript.transform);
    //        newResObj.name = "new Text";
    //        newResObj.transform.localPosition = Vector3.zero;

    //        info.TextObj = newResObj.GetComponent<Text>();
    //    }

    //    //void AddResourceInfo_BallonImage(StepInfo info)
    //    //{
    //    //    GameObject resourcePrefab = Resources.Load(StoryMakerManager.ImagePrefabPath) as GameObject;
    //    //    if (null == resourcePrefab)
    //    //        return;

    //    //    GameObject newResObj = Instantiate(resourcePrefab, _propScript.transform);
    //    //    newResObj.name = "new TextBallon";
    //    //    newResObj.transform.localPosition = Vector3.zero;

    //    //    info.TextBallonImageObj = newResObj.GetComponent<Image>();
    //    //}

    //    /// <summary>
    //    /// 이미지 리소스 추가
    //    /// </summary>
    //    void AddResourceInfo_Image()
    //    {
    //        GameObject resourcePrefab = Resources.Load(StoryMakerManager.ImagePrefabPath) as GameObject;
    //        if (null == resourcePrefab)
    //            return;

    //        GameObject newResObj = Instantiate(resourcePrefab, _propScript.transform);
    //        newResObj.name = "new Image";
    //        newResObj.transform.localPosition = Vector3.zero;
    //    }

    //    /// <summary>
    //    /// StepInfo 관련 버튼
    //    /// </summary>
    //    void OnDraw_StepButtons(StepInfo info)
    //    {
    //        //if (GUILayout.Button("ADD String Ani Text"))
    //        //{
    //        //    AddResourceInfo_Text(info);
    //        //}
    //        //if (GUILayout.Button("ADD TextBallon Image"))
    //        //{
    //        //    AddResourceInfo_BallonImage(info);
    //        //}

    //        /// 스탭인포 삭제
    //        GUI.color = Color.red;
    //        if (GUILayout.Button("Delete"))
    //        {
    //            _propScript.StepList.Remove(info);
                
    //            Transform parentObj = info.TextBallonImageObj.transform.parent;
    //            if(null != parentObj)
    //                DestroyImmediate(parentObj.gameObject);
    //        }
    //        GUI.color = Color.white;

    //        GUILayout.Space(3);
    //    }
        
    //    /// <summary>
    //    /// 이번 스탭에서 사용될 말풍선 이미지와 텍스트를 연결해줌
    //    /// </summary>
    //    void OnDraw_ResourceSetting(StepInfo info)
    //    {
    //        info.TextBallonImageObj = (Image)EditorGUILayout.ObjectField("Ballon Image Object", info.TextBallonImageObj, typeof(Image), true);
    //        info.TextObj = (Text)EditorGUILayout.ObjectField("Ani Text Object", info.TextObj, typeof(Text), true);
    //    }

    //    /// <summary>
    //    /// 활성화 및 연출 시간
    //    /// </summary>
    //    void OnDraw_Duration(StepInfo info)
    //    {
    //        GUILayout.BeginHorizontal();
            
    //        info.Duration = EditorGUILayout.FloatField("Duration", info.Duration); /*GUILayout.HorizontalSlider(info.Duration, 0, 10);*/

    //        GUILayout.EndHorizontal();

    //        info.EndString = EditorGUILayout.TextField("End String", info.EndString);
    //    }

    //    /// <summary>
    //    /// 이번 스탭에 이미지를 사용할 것인지
    //    /// </summary>
    //    void OnDraw_UseImage(StepInfo info)
    //    {
    //        info.IsUseOtherImage = EditorGUILayout.Toggle("Is Use Image", info.IsUseOtherImage);
    //        if (info.IsUseOtherImage == false)
    //            info.OtherDirectionInfos.Clear();
    //    }
    //}
}
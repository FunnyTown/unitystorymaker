﻿using UnityEngine;

namespace StoryMaker
{
    public class HeaderNode : BaseNode
    {
        public void Init(float posX = 0, float posY = 0)
        {
            Type = NodeType.Start;
            DrawRect = new Rect(20, 50, 80, 80);

            if (0 != posX)
                DrawRect.x = posX;
            if (0 != posY)
                DrawRect.y = posY;
        }

        public override void OnDrawNode()
        {
            GUI.Box(StoryMakerManager.GetBackGroundBoxPos(DrawRect), "", "flow node 3");

            if (GUI.Button(StoryMakerManager.GetRightButtonPos(DrawRect), GetTexture(StoryMakerManager.Texture_NodeWhite)))
            {
                NodeSelectAction_Right(this);
            }
            
            GUILayout.BeginArea(GetMyRect());
            GUILayout.Space(30);
            GUILayout.Label("   START", "BoldLabel", GUILayout.Width(70));

            base.OnDrawNode();

            GUILayout.EndArea();
        }
    }
}
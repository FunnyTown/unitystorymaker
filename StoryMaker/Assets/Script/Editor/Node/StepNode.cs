﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace StoryMaker
{
    public class StepNode : BaseNode
    {
        public StepInfo SelectedStep;
        private Text _ballonText;
        
        GUIStyle TextEditorStyle;

        public void Init(float posX = 0, float posY = 0)
        {
            base.Init();

            Type = NodeType.End;
            DrawRect = new Rect(20, 50, 220, 152);

            if (0 != posX)
                DrawRect.x = posX;
            if (0 != posY)
                DrawRect.y = posY;
            if(null == TextEditorStyle)
            {
                TextEditorStyle = new GUIStyle();
                TextEditorStyle.normal.background = MakeTex(1, 1, new Color(60f / 256f, 60f / 256f, 60f / 256f, 1f));
                TextEditorStyle.richText = true;
                TextEditorStyle.normal.textColor = Color.white;
                TextEditorStyle.clipping = TextClipping.Clip;
            }
        }
        
        public override void OnDrawNode()
        {
            GUI.Box(StoryMakerManager.GetBackGroundBoxPos(DrawRect), "", "flow node 3");

            if (GUI.Button(StoryMakerManager.GetLeftButtonPos(DrawRect), GetTexture(StoryMakerManager.Texture_NodeYellow)))
            {
                NodeSelectAction_Left(this);
            }

            GUILayout.BeginArea(GetMyRect());

            /// 디스크립션 떄매 위에서 3픽셀만큼 띄우고 그리기 시작
            GUILayout.Space(3);

            OnDraw_TitleAndDiscription();
            OnDraw_SetpObj();
            OnDraw_Duration();
            OnDraw_TextID();
            OnDraw_EndString();

            base.OnDrawNode();

            GUILayout.EndArea();

            UpdateBallonText();
        }

        void OnDraw_TitleAndDiscription()
        {
            EditorGUILayout.BeginHorizontal();
            
            OnDraw_NodeState(ErrorCheck());
            EditorGUILayout.TextArea("", StoryMakerManager.GUIStyle_DiscriptionHeight);
            
            EditorGUILayout.EndHorizontal();
        }

        private bool ErrorCheck()
        {
            if (null != SelectedStep.BallonObj)
            {
                Transform childe;
                childe = SelectedStep.BallonObj.transform.Find("ImageBallon");
                if (null == childe)
                    return true;
                else
                {
                    if (null == childe.transform.Find("ImageTouch"))
                        return true;
                }

                childe = SelectedStep.BallonObj.transform.Find("TextString");
                if (null == childe)
                    return true;
            }

            if (SelectedStep.BallonObj == null || SelectedStep.Duration == 0f || SelectedStep.EndString == string.Empty)
            {
                return true;
            }

            if(SelectedStep.BallonObj != null)
            {
                TextBallonResize com = SelectedStep.BallonObj.GetComponent<TextBallonResize>();
                if (null == com)
                    return true;
            }

            if (SelectedStep.EndStringID <= 0)
                return true;

            return false;
        }

        public void UpdateBallonText()
        {
            if (null == _ballonText)
            {
                if(null != SelectedStep.TextObj)
                    _ballonText = SelectedStep.TextObj;
            }

            if (null == _ballonText)
                return;

            _ballonText.text = SelectedStep.EndString;
        }

        public void OnDraw_AutoResize()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("IsAutoResize", StoryMakerManager.GUIStyle_NodeTextWidth);
            SelectedStep.IsAutoResize = EditorGUILayout.Toggle(SelectedStep.IsAutoResize);

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_Duration()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Duration (s)", StoryMakerManager.GUIStyle_NodeTextWidth);
            SelectedStep.Duration = EditorGUILayout.FloatField(SelectedStep.Duration, GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_SetpObj()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Step Obj", StoryMakerManager.GUIStyle_NodeTextWidth);
            SelectedStep.BallonObj = (GameObject)EditorGUI.ObjectField(new Rect(89, 35, 100, 15), "", SelectedStep.BallonObj, typeof(GameObject), true);

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_EndString()
        {
            GUILayout.Space(3f);
            
            if(null != TextEditorStyle)
            {
                SelectedStep.EndString = EditorGUILayout.TextArea(SelectedStep.EndString, TextEditorStyle, GUILayout.Height(55));
            }
        }

        public void OnDraw_TextID()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("String ID", StoryMakerManager.GUIStyle_NodeTextWidth);
            SelectedStep.EndStringID = EditorGUILayout.IntField(SelectedStep.EndStringID, GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();
        }

        private Texture2D MakeTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];
            for (int i = 0; i < pix.Length; ++i)
            {
                pix[i] = col;
            }
            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();
            return result;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace StoryMaker
{
    public class BaseNode
    {
        protected Rect ChildeRect;
        public string Discription = "Discription ... ";
        public DirectionInfo DirectionInfo;
        public Rect DrawRect;
        public NodeType Type;

        public BaseNode FrontNode { get; set; }
        public BaseNode NextNode { get; set; }

        public List<BaseNode> NextMultiNode = new List<BaseNode>();

        public bool IsDrag = false;
        public bool IsDrawLine = false;

        public Action<BaseNode> NodeSelectAction_Left;
        public Action<BaseNode> NodeSelectAction_Right;

        public GUIStyle TitleTextStyle_Error;
        public GUIStyle TitleTextStyle_None;

        Vector2 _mousePos;
        
        public void OnDraw_DefaultUI()
        {
            GUI.Box(StoryMakerManager.GetBackGroundBoxPos(DrawRect), "", StoryMakerManager.GetNodeContent(DirectionInfo.IsMultiPlay, DirectionInfo.IsFreeze));

            if (GUI.Button(StoryMakerManager.GetLeftButtonPos(DrawRect), GetTexture(StoryMakerManager.Texture_NodeYellow)))
            {
                NodeSelectAction_Left(this);
            }

            if(DirectionInfo.IsMultiPlay == false)
            {
                if (GUI.Button(StoryMakerManager.GetRightButtonPos(DrawRect), GetTexture(StoryMakerManager.Texture_NodeWhite)))
                {
                    NodeSelectAction_Right(this);
                }
            }
            else
            {
                NextNode = null;
            }
        }

        public virtual void Init()
        {
            GUIStyle boldLabel = new GUIStyle();
            boldLabel = "BoldLabel";

            if (null == TitleTextStyle_Error)
            {
                TitleTextStyle_Error = new GUIStyle();
                TitleTextStyle_Error.font = boldLabel.font;
                TitleTextStyle_Error.padding = boldLabel.padding;
                TitleTextStyle_Error.margin = boldLabel.margin;
                TitleTextStyle_Error.richText = true;
                TitleTextStyle_Error.normal.textColor = Color.red;
            }
            
            if(null == TitleTextStyle_None)
            {
                TitleTextStyle_None = new GUIStyle();
                TitleTextStyle_None.font = boldLabel.font;
                TitleTextStyle_None.padding = boldLabel.padding;
                TitleTextStyle_None.margin = boldLabel.margin;
                TitleTextStyle_None.richText = true;
                TitleTextStyle_None.normal.textColor = Color.white;
            }
        }

        public virtual void OnDrawNode()
        {
            NodeMoveEvent();
        }

        public void MoveForSliderValue()
        {

        }

        public void NodeMoveEvent()
        {
            if (IsDrawLine == true)
                return;

            _mousePos = Event.current.mousePosition;
            _mousePos.x += DrawRect.x;
            _mousePos.y += DrawRect.y;

            if (Event.current != null && Event.current.button == 0)
            {
                switch (Event.current.type)
                {
                    case EventType.MouseDown:
                        if (DrawRect.Contains(_mousePos))
                        {
                            IsDrag = true;
                        }
                        break;
                    case EventType.MouseDrag:
                        if (IsDrag)
                        {
                            DrawRect.x += Event.current.delta.x;
                            DrawRect.y += Event.current.delta.y;
                        }
                        break;
                    case EventType.MouseUp:
                        if (IsDrag)
                        {
                            IsDrag = false;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        protected Rect GetMyRect()
        {
            return ChildeRect = new Rect(DrawRect.x + 5, DrawRect.y, DrawRect.width - 10, DrawRect.height);
        }

        public Texture GetTexture(string path)
        {
            Texture findTexture = AssetDatabase.LoadAssetAtPath(path, typeof(Texture)) as Texture;
            if (null == findTexture)
                return null;

            return findTexture;
        }

        public Rect GetDrawLineStartPos()
        {
            Rect returnRect = new Rect(0, 0, 0, 0);
        
            returnRect.x = DrawRect.x + DrawRect.width+10;
            returnRect.y = DrawRect.y + (DrawRect.height / 2);

            return returnRect;
        }

        public Rect GetDrawLineEndPos()
        {
            Rect returnRect = new Rect(0, 0, 0, 0);

            returnRect.x = DrawRect.x-10;
            returnRect.y = DrawRect.y + (DrawRect.height / 2);

            return returnRect;
        }

        public bool IsMulti()
        {
            if (DirectionInfo == null)
                return false;

            return DirectionInfo.IsMultiPlay;
        }

        public void Check_ChangeMultiState()
        {
            //foreach (BaseNode nodeinfo in NextMultiNode)
            //{
            //    if (false == nodeinfo.IsMulti())
            //        NextMultiNode.Remove(nodeinfo);
            //}
        }

        public void OnDraw_NodeState(bool errorState)
        {
            if(true == errorState)
            {
                GUILayout.Label(GetNodeTypeString(), TitleTextStyle_Error, GUILayout.Width(GetNodeTypeWidth()));
            }
            else
            {
                GUILayout.Label(GetNodeTypeString(), TitleTextStyle_None, GUILayout.Width(GetNodeTypeWidth()));
            }
        }

        private string GetNodeTypeString()
        {
            switch(Type)
            {
                case NodeType.Active: return "Active";
                case NodeType.Bounce: return "Bounce";
                case NodeType.Color: return "Color";
                case NodeType.Enable: return "Enable";
                case NodeType.FadeInOut: return "FadeInOut";
                case NodeType.Jump: return "Jump";
                case NodeType.Move: return "Move";
                case NodeType.Rotation: return "Rotation";
                case NodeType.Scale: return "Scale";
                case NodeType.Shake: return "Shake";
                case NodeType.SpineAni: return "Spine Ani";
                case NodeType.End: return "TextBallon";
                case NodeType.Swap: return "Swap";
                default: return "";
            }
        }

        private int GetNodeTypeWidth()
        {
            switch (Type)
            {
                case NodeType.Active: return 50;
                case NodeType.Bounce: return 60;
                case NodeType.Color: return 50;
                case NodeType.Enable: return 90;
                case NodeType.FadeInOut: return 80;
                case NodeType.Jump: return 50;
                case NodeType.Move: return 50;
                case NodeType.Rotation: return 50;
                case NodeType.Scale: return 50;
                case NodeType.Shake: return 50;
                case NodeType.SpineAni: return 50;
                case NodeType.End: return 80;
                case NodeType.Swap: return 50;
                default: return 80;
            }
        }

        public void OnDraw_MultiBoolean()
        {
            EditorGUILayout.BeginHorizontal();
            
            GUILayout.Label("IsMulty", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.IsMultiPlay = EditorGUILayout.Toggle(DirectionInfo.IsMultiPlay);
            
            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_Active()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Active", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.ActiveType = (ActiveType)EditorGUILayout.EnumPopup(DirectionInfo.ActiveType);

            EditorGUILayout.EndHorizontal();
        }
        
        /// <summary>
        /// TODO jaewoo - 스왑 관련 수정해야함
        /// </summary>
        public void OnDraw_SwapObjField()
        {
            EditorGUILayout.BeginHorizontal();
            
            GUILayout.Label("Swap Obj", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.SwapObj = (GameObject)EditorGUILayout.ObjectField(DirectionInfo.SwapObj, typeof(GameObject), true, GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_SwapImageField()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Swap Image", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.SwapImage = (Sprite)EditorGUILayout.ObjectField(DirectionInfo.SwapImage, typeof(Sprite), true, GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_DurationField()
        {
            EditorGUILayout.BeginHorizontal();
            
            GUILayout.Label("Duration (s)", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.Duration = EditorGUILayout.FloatField(DirectionInfo.Duration, GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();
        }
        
        public void OnDraw_EndPos()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("End Position", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.EndPos = EditorGUILayout.Vector2Field("", DirectionInfo.EndPos, GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_Color()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Change Color", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.Color = EditorGUILayout.ColorField( DirectionInfo.Color, GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_JumpHeight()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Jump Height", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.JumpHeight = EditorGUILayout.FloatField(DirectionInfo.JumpHeight, GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_Rotation()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Rotate", GUILayout.Width(55));
            DirectionInfo.EndRotation = EditorGUILayout.Vector3Field("", DirectionInfo.EndRotation, GUILayout.Width(130));

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_TargetObj()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Target", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.TargetObj = (GameObject)EditorGUILayout.ObjectField(DirectionInfo.TargetObj, typeof(GameObject), true, GUILayout.Width(100));

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_Scale()
        {
            EditorGUILayout.BeginHorizontal();

            if (0 == DirectionInfo.EndScale.x) DirectionInfo.EndScale.x = 1;
            if (0 == DirectionInfo.EndScale.y) DirectionInfo.EndScale.y = 1;
            if (0 == DirectionInfo.EndScale.z) DirectionInfo.EndScale.z = 1;

            GUILayout.Label("Scale", GUILayout.Width(55));
            DirectionInfo.EndScale = EditorGUILayout.Vector3Field("", DirectionInfo.EndScale, GUILayout.Width(130));

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_FadeType()
        {
            EditorGUILayout.BeginHorizontal();
            
            GUILayout.Label("FadeInOut", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.FadeType = (FadeInOutType)EditorGUILayout.EnumPopup(DirectionInfo.FadeType);
            
            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_IsFreezeBoolean()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("IsFreeze", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.IsFreeze = EditorGUILayout.Toggle(DirectionInfo.IsFreeze);

            EditorGUILayout.EndHorizontal();
        }
        
        public void OnDraw_MoveStartPos()
        {
            EditorGUILayout.Space();
            GUILayout.Label("Start Pos", "ProfilerBadge", GUILayout.Width(60));

            EditorGUILayout.BeginHorizontal();

            if (null != DirectionInfo.TargetObj)
            {
                if (GUILayout.Button("GET"))
                {
                    DirectionInfo.StartPos = DirectionInfo.TargetObj.GetComponent<RectTransform>().anchoredPosition;
                }
                if (GUILayout.Button("SET"))
                {
                    DirectionInfo.TargetObj.GetComponent<RectTransform>().anchoredPosition = DirectionInfo.StartPos;
                }

                DirectionInfo.StartPos = EditorGUILayout.Vector2Field("", DirectionInfo.StartPos, GUILayout.Width(100));
            }
            else
            {
                GUILayout.Label("Target Object is Null", "CN StatusWarn", GUILayout.Width(150));
            }
            
            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_MoveEndPos()
        {
            EditorGUILayout.Space();
            GUILayout.Label("End Pos", "ProfilerBadge", GUILayout.Width(60));

            EditorGUILayout.BeginHorizontal();

            if (null != DirectionInfo.TargetObj)
            {
                if (GUILayout.Button("GET"))
                {
                    DirectionInfo.EndPos = DirectionInfo.TargetObj.GetComponent<RectTransform>().anchoredPosition;
                }
                if (GUILayout.Button("SET"))
                {
                    DirectionInfo.TargetObj.GetComponent<RectTransform>().anchoredPosition = DirectionInfo.EndPos;
                }

                DirectionInfo.EndPos = EditorGUILayout.Vector2Field("", DirectionInfo.EndPos, GUILayout.Width(100));
            }
            else
            {
                GUILayout.Label("Target Object is Null", "CN StatusWarn", GUILayout.Width(150));
            }

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_AutoText()
        {
            EditorGUILayout.Space();
            GUILayout.Label("Auto Option", "ProfilerBadge", GUILayout.Width(190));
        }

        public void OnDraw_IsAutoActiveFalse()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Is Auto Active False", GUILayout.Width(160));
            DirectionInfo.IsAutoActiveFalue = EditorGUILayout.Toggle(DirectionInfo.IsAutoActiveFalue);

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_IsGradation()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Is Auto Gradation", GUILayout.Width(160));
            DirectionInfo.IsGradationStepEnd = EditorGUILayout.Toggle(DirectionInfo.IsGradationStepEnd);

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_IsAutoColorWhite()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Is Auto Color White", GUILayout.Width(160));
            DirectionInfo.IsAutoColorWhite = EditorGUILayout.Toggle(DirectionInfo.IsAutoColorWhite);

            EditorGUILayout.EndHorizontal();
        }

        public void OnDraw_Enable()
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Enable", StoryMakerManager.GUIStyle_NodeTextWidth);
            DirectionInfo.EnableType = (EnableType)EditorGUILayout.EnumPopup(DirectionInfo.EnableType);

            EditorGUILayout.EndHorizontal();
        }
    }
}



﻿using UnityEditor;
using UnityEngine;

namespace StoryMaker
{
    public class ShakeNode : BaseNode
    {
        public override void Init()
        {
            base.Init();
            Type = NodeType.Shake;
            DrawRect = new Rect(20, 50, 200, 130 + 10);
        }

        public override void OnDrawNode()
        {
            OnDraw_DefaultUI();

            GUILayout.BeginArea(GetMyRect());

            /// 디스크립션 떄매 위에서 3픽셀만큼 띄우고 그리기 시작
            GUILayout.Space(3);

            OnDraw_TitleAndDiscription();
            OnDraw_MultiBoolean();
            OnDraw_TargetObj();
            OnDraw_DurationField();

            OnDraw_AutoText();
            OnDraw_IsAutoActiveFalse();
            //OnDraw_IsGradation();
            //OnDraw_IsAutoColorWhite();

            base.OnDrawNode();

            GUILayout.EndArea();

            Check_ChangeMultiState();
        }

        void OnDraw_TitleAndDiscription()
        {
            EditorGUILayout.BeginHorizontal();

            OnDraw_NodeState(ErrorCheck());
            DirectionInfo.Discription = EditorGUILayout.TextArea(DirectionInfo.Discription, "ScriptText", StoryMakerManager.GUIStyle_DiscriptionHeight);

            EditorGUILayout.EndHorizontal();
        }

        private bool ErrorCheck()
        {
            if (DirectionInfo.TargetObj == null || DirectionInfo.Duration == 0f)
            {
                return true;
            }
            return false;
        }
    }
}
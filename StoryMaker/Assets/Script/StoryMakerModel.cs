﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace StoryMaker
{
    public class StoryMakerModel : MonoBehaviour
    {
        [HideInInspector] public StoryProp selectProp;
        public Button ButtonObj;
        
        public void Awake()
        {
            ButtonObj.onClick.AddListener(OnClick);
            ButtonObj.transform.SetAsLastSibling();
            FindPlayProp();
        }

        public void OnClick()
        {
            if (null != ButtonObj)
                ButtonObj.gameObject.SetActive(false);

            StartCoroutine(Play());
        }

        public IEnumerator Play()
        {
            yield return StartCoroutine(selectProp.StartStoryProp());
        }
        

        public void FindPlayProp()
        {

            GameObject findProps = GameObject.FindGameObjectWithTag("StoryProp");
            if (null != findProps)
                selectProp = findProps.GetComponent<StoryProp>();
        }

        #region Use Editer Mode

        public StoryProp GetSelectProp()
        {
            if (null == selectProp)
                return null;

            return selectProp;
        }

        public Transform GetImageParent()
        {
            if (null == selectProp)
                return null;

            return selectProp.transform.Find("Images");
        }

        public Transform GetTextParent()
        {
            if (null == selectProp)
                return null;

            return selectProp.transform.Find("Texts");
        }

        public Transform GetTextBallonParent()
        {
            if (null == selectProp)
                return null;

            return selectProp.transform.Find("TextBallons");
        }

        public Transform GetSpineParent()
        {
            if (null == selectProp)
                return null;

            return selectProp.transform.Find("Spines");
        }

        public void SetSelectProp(StoryProp propItem)
        {
            selectProp = propItem;
        }
        
        public List<StepInfo> GetStepList()
        {
            return selectProp.StepList;
        }

        public StepInfo GetFirstStepInfo()
        {
            if (null == selectProp)
                return null;
            else
            {
                StepInfoObject[] StepObjts = GetSelectProp().GetComponentsInChildren<StepInfoObject>();
                return StepObjts[0].GetComponent<StepInfo>();
            }
        }

        #endregion
    }
}

